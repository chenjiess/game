<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2019/10/29
  Time: 14:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="wrapper" style="width:1200px;">
    <div style="height: 15px; clear: both; overflow: hidden">
    </div>
    <dl id="position">
        <dt>您当前的位置：<a href="">Stone首页</a></dt><dd>&gt;<a href="javascript:void(0)" onclick="myStone()">我的Stone</a></dd><dd>&gt;<span id="UserMap"></span></dd>
    </dl>
    <div id="welcome">
    </div>

    <div id="side-nav">
        <div class="my_vancl">
            <h2>
                <a href="javascript:void(0)" onclick="myStone()" class="track" name="my-left-left-commorder" >我的Stone</a></h2>
        </div>
        <ul class="">
            <li>
                <h3>
                    订单中心</h3>
            </li>
            <li class="">
                <a href="javascript:void(0)" class="track" name="my-left-left-Order" >
                    ·我的订单</a>
            </li>
            <li class="">
                <a href="javascript:void(0)" class="track" name="my-left-left-card" onclick="collection()">
                    ·我的收藏</a>
            </li>
            <li class="no_bg">
                <a href="javascript:void(0)" class="track" name="my-left-left-huiyuanscore" onclick="shoppingCart()">
                    ·我的购物车</a>
            </li>
        </ul>
        <ul class="">
            <li>
                <h3>
                    客户服务</h3>
            </li>
            <li class="">
                <a href="" class="track" name="my-left-left-returnorder" itemUrl="http://returns.vancl.com/" url="http://my.vancl.com/">
                    ·退换货办理</a>
            </li>
            <li class="">
                <a href="" class="track" name="my-left-left-productcomment" itemUrl="http://my.vancl.com/comment/" url="http://my.vancl.com/">
                    ·我要评价</a>
            </li>
            <li class="">
                <a href="" class="track" name="my-left-left-productquestion" itemUrl="http://my.vancl.com/question" url="http://my.vancl.com/">
                    ·商品提问</a>
            </li>
        </ul>
        <ul class="service-rate">
            <li>
                <h3>
                    账户管理</h3>
            </li>
            <li class="">
                <a href="" class="track" name="my-left-left-account" itemUrl="http://my.vancl.com/user/account" url="http://my.vancl.com/">
                    ·账户余额</a>
            </li>
            <li class="">
                <a href="" class="track" name="my-left-left-modifyDeliveryInfo" itemUrl="http://my.vancl.com/DeliveryAddress/Index" url="http://my.vancl.com/">
                    ·收货地址</a>
            </li>
            <li class="">
                <a href="" class="track" name="my-left-left-modifypassword" itemUrl="http://my.vancl.com/SafeCenter" url="http://my.vancl.com/">
                    ·账户安全</a>
            </li>
            <li class="">
                <a href="" class="track" name="my-left-left-personinfo" itemUrl="http://my.vancl.com/User/UserInfo" url="http://my.vancl.com/">
                    ·我的资料</a>
            </li>
            <li class="no_bg">
                <a href="" class="track" name="my-left-left-assoaccount" itemUrl="http://my.vancl.com/federatedlogin" url="http://my.vancl.com/">
                    ·我的关联账户</a>
            </li>
        </ul>
    </div>
