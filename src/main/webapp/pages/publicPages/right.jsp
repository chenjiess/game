<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2019/10/29
  Time: 14:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="main-content-right" style="width: 80%; float: left;">
    <iframe id="iframe"  name="rightFrame"  width="100%" height="100%" src="" οnlοad="this.height=iframe.document.body.scrollHeight"
             scrolling="no" frameborder=0>
    </iframe>
</div>
<script type="text/javascript" src="/Game/js/jquery-1.12.4.js"></script>
<script type="text/javascript">
    function getUrlParam(key) {
            // 获取参数
            var url = window.location.search;
            // 正则筛选地址栏
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
            // 匹配目标参数
            var result = url.substr(1).match(reg);
            //返回参数值
            return result ? decodeURIComponent(result[2]) : null;
        }
        var pages = getUrlParam("pages");
            pages = pages==null?1:pages;
        var id = <%=session.getAttribute("id")%>;
            id = id == null?1:id;
        var url = "personMsg/myStone.jsp?id=" + id;
        if (pages == "3"){
            url = "personMsg/shoppingCart.jsp?id=" + id;
        }
        document.getElementById("iframe").src = url;
        function collection() {
            document.getElementById("iframe").src = "personMsg/collection.jsp?id=" + id;
            document.getElementById("UserMap").innerHTML = "我的收藏";
        }
        function myStone() {
            document.getElementById("iframe").src = "personMsg/myStone.jsp?id=" + id;
            document.getElementById("UserMap").innerHTML = "";
        }
        function shoppingCart(){
            document.getElementById("iframe").src = "personMsg/shoppingCart.jsp?id=" + id;
            document.getElementById("UserMap").innerHTML = "我的购物车";
        }
</script>
