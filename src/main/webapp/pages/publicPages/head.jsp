<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="/Game/css/public/head.css"/>
<div id="Head" class="vanclHead">
    <div id="headerTopArea" class="headerTopAreaBox">
        <div class="headerTopArea">
            <div class="headerTop">
                <div class="headerTopRight" style="width: 126px;">
                    <div class="active notice" id="vanclCustomer">
                        <a class="mapDropTitle" href="" target="_blank">网站公告</a>
                    </div>
                    <div class="payattention"><em></em>
                        <a href="" class="vweixinbox"><span class="vweixin" style="background: url(../../img/public/weixin.jpg) no-repeat left 3px;"></span><b class="vweixinTip"></b></a>
                        <a href="" class="track vanclweibo" name="hp-hp-head-weibo-v:n" target="_blank" style="background: url(../../img/public/weibo.jpg) no-repeat left 3px;"></a>
                    </div>
                </div>
                <div class="headerTopLeft">
                    <div id="welcome" class="top loginArea"> 您好,<span class="top">欢迎光临Stone！&nbsp;</span><span><a href="../loginAndResign.html" name="hp-hp-head-signin-v:n" class="top track">登录</a>&nbsp;|&nbsp;<a href="javascript:location.href='../loginAndResign.html?'+encodeURIComponent(location.href)" name="hp-hp-head-register-v:n" class="track">注册</a></span></div>
                    <div class="recommendArea">
                        <a href="//my.vancl.com/Order/" rel="nofollow" class="track" name="hp-hp-head-order-v:n"> 我的订单</a>
                    </div>
                </div><span class="blank0"></span></div>
        </div>
    </div>
    <div id="logoArea" class="vanclLogoSearch">
        <div class="vanclSearch fr">
            <div class="searchTab">
                <div class="search fl"><input type="text" class="searchText ac_input textBox Enter fl" name="k" id="skey" value="" defaultkey="运动户外" /><input type="button" class="searchBtn sousuoBtn btn" id="btnHeaderSearch" onfocus="this.blur()" /></div>
                <div class="buycar fr active" id="shoppingCarNone">
                    <p>
                        <a id="shoppingcar_link" rel="nofollow" href="/Game/pages/personMsg.jsp?pages=3" name="hp-hp-head-cart-v:n:t" class="shopborder track cartab">购物车(<span car_product_total="shoppingCar_product_totalcount">0</span>)</a><s></s></p>
                    <div class="bottomlines"></div>
                    <div class="BuycarTab shopDropList"></div>
                </div>
            </div>
            <div class="hotWord">
                <p> 热门搜索：
                    <a name="hp-hp-classhotsearch-1_1-v:n" class="track" href="" target="_blank">1</a>
                    <a name="hp-hp-classhotsearch-1_2-v:n" class="track" href="" target="_blank">1</a>
                    <a name="hp-hp-classhotsearch-1_3-v:n" class="track" href="" target="_blank">2</a>
                    <a name="hp-hp-classhotsearch-1_4-v:n" class="track" href="" target="_blank">3</a>
                    <a name="hp-hp-classhotsearch-1_5-v:n" class="track" href="" target="_blank">4</a>
                    <a name="hp-hp-classhotsearch-1_6-v:n" class="track" href="" target="_blank">5</a>
                    <a name="hp-hp-classhotsearch-1_7-v:n" class="track" href="" target="_blank">6</a>
                </p>
            </div>
        </div>
    </div>
    <div class="navlist clear" id="mainNavBox" style="z-index:300!important;">
        <ul>
            <li class="vancllogo_Con" style="text-align: left;">
                <a href=""></a>
            </li>
            <li>
                <a href="" class="track">首页</a><span class="NavLine"></span></li>
            <li>
                <a href="" class="track" name="hp-hp-head-nav_1-v:n" target="_blank">type1</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="" class="track" name="hp-hp-head-nav_4-v:n" target="_blank">type2</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="" class="track" name="hp-hp-head-nav_5-v:n" target="_blank">type3</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="" class="track" name="hp-hp-head-nav_2-v:n" target="_blank">type4</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="" class="track" name="hp-hp-head-nav_6-v:n" target="_blank">type5</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="" class="track" name="hp-hp-head-nav_7-v:n" target="_blank">type6</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="" class="track" name="hp-hp-head-nav_8-v:n" target="_blank">type7</a><span class="NavLine"></span>
            </li>
            <li class="none">
                <a href="" class="track" name="hp-hp-head-nav_9-v:n" target="_blank">type8</a><span class="NavLine"></span>
            </li>
            <li>
                <a href="" class="track" name="hp-hp-head-nav_10-1-v:n" target="_blank">type9</a><span class="NavLine"></span>
            </li>
        </ul>
    </div>
</div>
