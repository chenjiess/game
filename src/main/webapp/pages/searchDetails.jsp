<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2019/10/29
  Time: 10:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <title>刺客信条:奥德赛</title>
    <!--<meta name="keywords" content="刺客信条:奥德赛，，VANCL凡客诚品" />
    <meta name="description" content="空，VANCL凡客诚品网上购物、送货上门、货到付款，无条件退换货。" />
    <link rel="shortcut icon" href="//i.vanclimg.com/common/favicon/favicon.ico" type="image/x-icon" />-->
    <link href="../css/details_2.css" type="text/css" rel="stylesheet"/>
    <style type="text/css">.SpriteColors {
        background-image: url(http://i.vanclimg.com/joinimages.ashx?d=/36/product/&href=6/3/7/637591/98493,6/3/7/637591/88488,6/3/7/637592/08501);
        width: 36px;
        height: 36px;
        display: inline-block;
        float: left;
    }</style>

    <link href="../css/details.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" src="../js/details.js"></script>
    <title>

    </title></head>
<div id="ProductTitleHide" class="danpinTitleTab newclear" style="display: none">


    <h2 title="刺客信条:奥德赛">
        刺客信条:奥德赛</h2>
    <div class="AddtoCar fr">


        <a href="#ProductTitleHide" class="cartab" id="hidCartab"></a>


    </div>
    <ul class="ProductSubnav fr" id="ItemTagFixed">

        <li><a href="#anchorPinglun">评论</a>|</li>
        <li><a href="#anchorQuiz">提问</a></li>
    </ul>

</div>

<div class="danpinBox">
    <input id="CustomerMade" type="hidden" value="NoMade"/>
    <a name="top"></a><span class="blank10"></span>


    <div class="breadNav">
        <a href="//www.vancl.com/" title="首页" name="nav">
            首页</a><span>&gt;</span>
        <a href="//s.vancl.com/27533.html" title="运动户外">
            运动户外</a><span>&gt;</span>
        <a href="//s.vancl.com/27688.html" title="男运动装">
            男运动装</a><span>&gt;</span>
        <a href="//s.vancl.com/27701.html" title="外套">
            外套</a><span>&gt;</span>
        <a href="//s.vancl.com/27702.html" title="户外外套">
            户外外套</a><span>&gt;</span>

        <input id="mainCategories" type="hidden" value="27533,27688,27701,27702"/>
        <span id="styleinfo" name="1067444">
                刺客信条:奥德赛</span>
    </div>

    <span class="blank0"></span>
    <div class="danpinArea">
        <div id="ProductTitleShow" class="danpinTitleTab newclear">


            <style type="text/css">
                #ProductTitleShow #productTitle .brandNumArea {
                    display: none;
                }
            </style>
            <div id="productTitle">
                <div class="brandNumArea">
                    <span id="productcode">商品编号：6375919</span></div>

                <h2 title="刺客信条:奥德赛">


                    刺客信条:奥德赛</h2>

                <ul class="ProductSubnav fr" id="ItemTag">

                    <li><a href="#anchorPinglun">评论</a>|</li>
                    <li><a href="#anchorQuiz">提问</a></li>
                </ul>
            </div>

        </div>
        <span class="blank30"></span>
        <div class="blank0">
        </div>
        <div class="danpin_colLef">


            <style type="text/css">
                .promotion-belt {
                    display: none;
                    flex: 1;
                    color: #fff;
                    font-family: "Microsoft YaHei";
                    position: absolute;
                    bottom: 0;
                    right: 0;
                    left: 0;
                    font-size: 16px;
                    height: 60px;
                }

                .promotion-belt-price {
                    display: flex;
                    flex-flow: row wrap;
                    background: #c72f2a;
                    flex: 1;
                    color: #fff;
                    padding: 8px 11px;
                    position: relative;
                }

                .promotion-belt-price .pointer {
                    display: flex;
                    position: absolute;
                    background: #c72f2a;
                    right: -4px;
                    top: 25px;
                    width: 12px;
                    height: 12px;
                    transform: rotate(45deg);
                }

                .promotion-belt-price .old-price {
                    display: flex;
                    flex: 0 0 100%;
                }

                .promotion-belt-price .after-charge {
                    display: flex;
                    flex: 1;
                    align-items: flex-end;
                    position: relative;
                    padding-left: 11px;
                }

                .promotion-belt-price .after-charge em {
                    font-size: 28px;
                    display: flex;
                    align-items: flex-end;
                    position: absolute;
                    right: 0px;
                    top: -11px;
                }

                .promotion-belt-countdown {
                    display: flex;
                    flex: 0 0 160px;
                    background: #feb13d;
                    flex-direction: column;
                    justify-content: center;
                    align-items: center;
                    padding: 8px 14px;
                    box-sizing: border-box;
                }

                .promotion-belt-countdown .countdown em {
                    color: #fff;
                    border-radius: 3px;
                    padding: 2px;
                }

                .promotion-belt-countdown .title {
                    color: #000;
                    margin-bottom: 5px;
                }

                .promotion-belt-countdown .countdown {
                    width: 90px;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    background: #be4b0f;
                    padding: 0px 6px;
                    border-radius: 5px;
                }

                .promotion-belt-price .mention {
                    display: flex;
                    flex: 1;
                    align-items: flex-end;
                }

                div#promotion-tag {
                    width: 50px;
                    height: 50px;
                    position: absolute;
                    left: 15px;
                    top: 15px;
                }

                div#promotion-tag img {
                    width: 100%;
                    height: 100%;
                }

                .promotion-belt.hide-countdown .promotion-belt-countdown {
                    display: none;
                }

                /*.promotion-belt.hide-countdown
                {
                    height: 90px;
                }


                .promotion-belt.hide-countdown .after-charge
                {
                    position: absolute;
                    bottom: 10px;
                    left: 10px;
                    margin: 0;
                }

                .promotion-belt.hide-countdown .old-price
                {
                    position: absolute;
                    bottom: 14px;
                    right: 20px;
                }*/
            </style>
            <div class="danpinColCenter">
                <div class="bigImg" id="vertical">

                    <img id="midimg" src="../img/Title.jpg" title="刺客信条:奥德赛"/>
                    <div id="winSelector" style="left: 0px; top: 0px; display: none;">
                    </div>

                </div>

            </div>


        </div>
        <div id="danpinRight" class="danpinRight" style="top: 0px">
            <div class="danpinfixedtitle">
                <h4 class="fl">
                    加入购物车
                </h4>
                <span class="close1 fr"></span>
            </div>
            <div class="danpinFixedLeftContent">
                <img id="danpinFixedLeftContentImg"
                     src="http://p2.vanclimg.com/product/6/3/7/6375919/mid/6375919-1j201704071345418493.jpg" title=""
                     alt=""/>
            </div>
            <div class="danpinFixedRightContent">
                <div name="Normal" id="pricearea">
                    <input id="shopid" type="hidden" value="10258"/>
                    <input id="hidattr" type="hidden" value="0"/>
                    <span class="blank10"></span>

                    <style type="text/css">
                        .priceLayout {
                            color: Black !important;

                        }

                        .priceLayout1 {
                            position: relative;
                            top: 1px;
                            margin-right: -5px;
                            margin-left: -5px;
                            *margin-right: 0px !important;
                            *margin-left: 0px !important;
                        }

                        .priceLayout2 {
                            position: relative;
                            top: 2px;
                        }

                        .tehuipricelayout {
                            *position: relative;
                            *top: 13px;
                        }

                        .tehuipricelayout1 {
                            *position: relative;
                            *top: 14px;
                        }

                        .tehuipricelayout2 {
                            *top: -1px;
                        }
                    </style>

                    <div class="cuxiaoPrice ">


     <span class="tehuiMoney" style="line-height: 26px;">

        <span>
            特惠价：</span><span style="font-family: '微软雅黑';">￥<strong>378.00</strong></span>




    </span>


                        <a id="czwenan" href="http://cz.vancl.com/DepositPre.aspx" target="_blank" style="float: left;
                                    height: 26px; display: inline-block; margin-left: 20px; line-height: 26px; margin-top: 7px;
                                    color: #a10000;">充100返100，点击充值</a>


                    </div>
                    <span class="blank10"></span>


                    <script language="javascript">
                        $("document").ready(function () {
                            $.ajax({
                                type: "get",
                                async: false,
                                url: "http://recom-s.vancl.com/RechargeCz/getRechargeCzInfo",
                                cache: false,
                                dataType: "jsonp",
                                jsonp: "callback",
                                success: function (data) {
                                    $("#czwenan").html("");
                                    $("#czwenan").html(data.OtherPage);
                                },
                                error: function (e) {
                                }
                            });
                        });
                    </script>
                </div>
                <div class="selectArea">
                    <div class="selColorArea">
                        <span class="blank10"></span>
                        <a id="colorlist"></a>
                        <!--<div class="danpinColor_Select" >
                            <div c lass="danpinColorTitle" style="line-height: 18px;">
                                <p>
                                    颜色：</p>
                            </div>
                            <div class="selColor">
                                <ul>
                                    <li  id="onlickColor"  name="6375919"  title="白色" >
                                        <div class="colorBlock" name="True">
                                            <a class="track" name="item-item-select-color_1" href='6375919.html?ref=item_color_6375919' >
                                                <span class="SpriteColors" style="background-position: 0px -0px">&nbsp;</span>
                                                <p>
                                                    白色</p>
                                                </a>
                                        </div>
                                        <span class="colorHoverok"></span>
                                    </li>
                                    <li name="6375918" class="colorBlocksouqing" title="午夜蓝 已售罄">
                                        <div class="colorBlock" name="True">
                                            <a class="track" name="item-item-select-color_2" href='6375918.html?ref=item_color_6375918_isoff' >
                                                <span class="SpriteColors" style="background-position: 0px -36px">&nbsp;</span>
                                                <p>
                                                    午夜蓝</p>
                                                </a>
                                        </div>
                                    </li>
                                    <li name="6375920" class="colorBlocksouqing"
                                        title="冰蓝色 已售罄">
                                        <div class="colorBlock" name="True">
                                            <a class="track" name="item-item-select-color_3" href='6375920.html?ref=item_color_6375920_isoff' >
                                                <span class="SpriteColors" style="background-position: 0px -72px">&nbsp;</span>
                                                <p>
                                                    冰蓝色</p>
                                                </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                                                    <div class="selSizeArea">


                        <div class="danpinColorTitle" >
                            <p>
                                尺码：</p>
                        </div>
                        <div class="selSize" >
                            <ul>

                                <li onclick="ChooseThisSize(this,'15773065',10)" name="15773065">
                                    <p>
                                        XL</p>
                                    </li>

                                <li onclick="ChooseThisSize(this,'15773066',10)" name="15773066">
                                    <p>
                                        XXL</p>
                                    </li>

                            </ul>
                        </div>-->

                        <div class="danpin_xuanzeGMcm" style="display:none;">
                            <span style="height: 16px; display: block; width: 16px; background-position: -1911px 0pt; margin: 2px;float: left; "
                                  class="sprites"></span>
                            <p>
                                请选择您要购买的商品尺码</p>
                        </div>

                    </div>
                    <div class="blank8ie">
                    </div>


                    <div class="goodsNum">
                        <div class="danpinColorTitle" style="line-height: 18px;">
                            <p>
                                数量：</p>
                        </div>
                        <div class="danpinnumSelect">
                            <select id="selectedAmount">

                                <option value="1">
                                    1
                                </option>

                                <option value="2">
                                    2
                                </option>

                                <option value="3">
                                    3
                                </option>

                                <option value="4">
                                    4
                                </option>

                                <option value="5">
                                    5
                                </option>

                                <option value="6">
                                    6
                                </option>

                                <option value="7">
                                    7
                                </option>

                                <option value="8">
                                    8
                                </option>

                                <option value="9">
                                    9
                                </option>

                                <option value="10">
                                    10
                                </option>

                            </select>
                            <span id="comeon" class="LastNum">余量有限</span> <span class="blank15"></span>
                        </div>
                    </div>
                    <span class="blank0"></span>
                    <div class="AreaItotal SelectGoods">
                        <div class="SelectAreaItotal SelectGoods">
                            <div class="goodsAddArea SelectGoods">
                                <div class="danpinColorTitle">
                                    <p class="SelectDetail">
                                        已选：</p> []
                                </div>
                                <div class="goodsAdd">
                                    <p class="SelectName">
                                    </p>
                                    <span class="SelectPoint">，</span>
                                    <p class="SelectSize">
                                    </p>
                                    <p class="NowHasGoods">
                                        现在有货</p>
                                    <span class="blank0"></span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="shoppingNews">
                        <a id="nowbuy" name="item-item-select-shopping" href="#"
                           class="btnnowbuy track"><span>立即购买</span></a><a id="addToShoppingCar"
                                                                           name="item-item-select-shopping"
                                                                           href="#" class="btnaddtocart track"></a>
                    </div>
                    <span class="blank20"></span>
                </div>
                <div class="blank0"></div>
            </div>
            <div id="promotion">

                <div class="danpin_YhTsBox danpin_YhTsTab ">
                    <h4>
                        <span>优惠提示</span></h4>
                    <ul>
                        <li style="background: none; padding-left: 0;">
					        	<span style="display: block; float: left;width: auto; height: 16px; background: #b81c22; margin: 4px 5px 0  0; color: #fff;
					            line-height: 16px; text-align: center; padding: 0 5px;">免邮</span>全场购物满199元免运费
                        </li>
                    </ul>
                </div>

            </div>
            <div class="blank15"></div>
        </div>

    </div>
</div>
<!--<script type="text/JavaScript" src="//jsy.vanclimg.com/js.ashx?href=/public/errorhandler.js,/jquery/jquery.fn.ALERT.js,/item/expressshopping.js,/item/styledetail.js,item/share.js,/item/changeproduct.js,/item/favorites.js,/sizecalculate/sliderclass.js,/item/ad.js,/index/common-header.js,/item/feedback.js,/item/feedback/impression.js,/item/interact.js&compress&v="></script>-->
<div id="reshouMainH"></div>
<input id="hdCategoryCode" type="hidden" value="1472"/>
<span class="blank20"></span>

<div class="sideBarSettabArea">
    <div class="RsetTabArea">
        <div id="product_set">
        </div>
        <div id="floatposition"></div>
        <span class="blank8"></span>


        <div class="itemtagcontentpart">
            <p><br/></p>

            <p>
                <input id="floatTitleHide" value="购买指南#gmzn,模特搭配#mtdp,面料特点#mltd,版型特点#bxtd,设计细节#sjxj,洗涤保养#xdby "
                       type="hidden"/>
            </p>
            <table id="gmzn" cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="../img/detail1.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="../img/detail2.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" align="center" id="mtdp">
                <tr>
                    <td><img src="../img/detail3.png" width="1200" height="183" alt="刺客信条:奥德赛"></td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td><img src="../img/detail4.jpg" width="1200" height="614" alt="刺客信条:奥德赛"></td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td><img src="../img/detail5.jpg" width="1200" height="607" alt="刺客信条:奥德赛"></td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td><img src="../img/thumbnails1.jpg" width="1200" height="719" alt="刺客信条:奥德赛"></td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td><img src="../img/thumbnails2.jpg" width="1200" height="721" alt="刺客信条:奥德赛"></td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td><img src="http://i2.vanclimg.com/cms/20170421/pfy_06.jpg" width="1200" height="713"
                             alt="刺客信条:奥德赛"></td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td><img src="http://i2.vanclimg.com/cms/20170421/pfy_07.jpg" width="1200" height="722"
                             alt="刺客信条:奥德赛"></td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td><img src="http://i2.vanclimg.com/cms/20170421/pfy_08.jpg" width="1200" height="721"
                             alt="刺客信条:奥德赛"></td>
                </tr>
            </table>
            <table id="mltd" cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_03.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_04.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table id="fwts" cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_05.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_06.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_07.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table id="mlxx" cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_08.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_09.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_10.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_11.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_12.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table id="cptx" cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_13.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table id="bxtd" cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_14.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_15.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table align="center" border="0" cellpadding="0" cellspacing="0">
                <tr class="firstRow">
                    <td><img src="http://i3.vanclimg.com/cms/20160603/kb.jpg" alt="" height="100" width="1200"/></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0" id="sjxj">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_16.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_17.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_18.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <tr class="firstRow">
                    <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_19.jpg" alt="刺客信条:奥德赛 "/></td>
                </tr>
                </tbody>
            </table>
            <table id="xdby" cellspacing="0" cellpadding="0" align="center" border="0">
                <tbody>
                <!--                <tr class="firstRow">
                      <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_20.jpg" alt="刺客信条:奥德赛 "/></td>
                    </tr>
                              </tbody>
                </table>
                            <table cellspacing="0" cellpadding="0" align="center" border="0">
                  <tbody>
                                <tr class="firstRow">
                      <td><img src="http://i4.vanclimg.com/cms/20170413/6375918_21.jpg" alt="刺客信条:奥德赛 "/></td>
                    </tr>
                              </tbody>
                </table>-->

                <script type="text/jscript">$(function () {        var sp = $.trim($('#floatTitleHide').val()).split(',');        if (sp.length <= 0) {            return;        }        var insetcontent = '';        for (var i = 0; i < sp.length; i++) {            if (sp[i].indexOf('#') >0) {                insetcontent += "<li><a href='#" + sp[i].split('#')[1] + "'>" + sp[i].split('#')[0] + "</a>|</li>";            }        }        $('#ItemTagFixed li:eq(0)').before(insetcontent);        $('#ItemTag li:eq(0)').before(insetcontent);
var tagArray=new Array();
            $('#ItemTagFixed').find("li").each(function(index){
                var obj= new Object();
                var liobj=$(this).find("a").first();
                obj.TagID=liobj.attr("href").substr(1);
                obj.TagText=liobj.html();
                obj.TagOrder= index;
                // 提问有可能为空
                obj.TagScrollTop=$("#"+obj.TagID).offset()!=undefined?$("#"+obj.TagID).offset().top:-1;
                tagArray.push(obj);
            })
            // 可能上面的标签和下面顺序不匹配，故重新排序
            tagArray.sort(function(a,b){return a.TagScrollTop-b.TagScrollTop});
            ///
            $(window).bind("scroll", function () {

                var doctop = $(document).scrollTop();
                for (var i=0;i<tagArray.length;i++)
                {
                    if(doctop>=tagArray[i].TagScrollTop && ( i==tagArray.length-1 || doctop < tagArray[i+1].TagScrollTop)) {
                        $(".ProductSubnav li a").eq(tagArray[i].TagOrder).addClass("selected");
                    }
                    else
                    {
                        $(".ProductSubnav li a").eq(tagArray[i].TagOrder).removeClass("selected");
                    }
                }
            });
});

                </script>
        </div>

        <style type="text/css">
            #anchor1 {
                width: 1px;
                height: 1px;
                visibility: hidden;
                position: absolute;
                top: 710px;
                *position: static;
                *display: block;
                *width: 1px;
                *height: 50px;
                *overflow: hidden;
                *top: none
            }


            #feedback {
                width: 1px;
                height: 1px;
                visibility: hidden;
                position: absolute;
                bottom: 40px;
                *position: static;
                *display: block;
                *width: 1px;
                *height: 30px;
                *overflow: hidden;
                *top: none;
            }
        </style>
        <div class="RsetTabCon">
            <span class="blank15"></span><a id="anchor1"></a>
            <div class="area1">
                <div style="position: relative; top: 0px; right: 0px; z-index: 0">

                    <span class="blank20"></span>

                    <div id="attr" style="display:none;">
                        <h3>
                            产品属性：(点击属性可查看同类商品)</h3>
                        <div class="dpShuXing">
                            <ul>

                            </ul>
                            <span class="blank0"></span>
                        </div>
                    </div>
                    <span class="blank20"></span>

                </div>

                <a id="feedback"></a>
            </div>
        </div>


        <span class="blank20"></span>
        <div class="RsetTabCon">
            <div class="productPinglun">
                <div style="width: 164px; float: left">
                    <div id="yushouMainS">

                    </div>
                    <div id="hotProduct" class="ptPinglunLef fl" style="width: 190px; padding: 0px; position: static;">
                        <h2 class="hotTitle" style="position: static; top: 0px; left: 0px; height: 40px;
                                    line-height: 40px; padding: 0px 0px 0px 25px; border-bottom: 1px solid #b4b4b4;">
                            推荐产品</h2>
                        <ul style="margin: 25px auto; width: 70%;">

                        </ul>
                    </div>
                </div>
                <div>
                    <div class="ptPinglunRig fr" style="width: 980px; padding: 0px;">
                        <a id="anchorPinglun">&nbsp</a>
                        <div class="NewComment">
                            <h2 class="hotTitle" style="position: static; top: 0px; left: 0px; height: 40px;
        line-height: 40px; padding: 0px 0px 0px 25px; border-bottom: 1px solid #b4b4b4;">

                                <span style="font-weight: bold;">最新评论(<span id="feedbackcount0">9</span>)</span>
                                <input id="hidPlCount" type="hidden" value="9"/>
                                <input id="AllPingLun" type="radio" style="margin-left: 5px;" name="ChoosePingLun"
                                       checked="checked" value="0" plcount="9"/><label for="AllPingLun"
                                                                                       style="margin-right: 5px;">全部评论</label>
                                <input id="PicPingLun" type="radio" name="ChoosePingLun" plcount="4"
                                       value="1"/><label for="PicPingLun">图片(4)</label>
                                <div class="pinglunTabRig fr" style="line-height: 40px; width: 120px; height: 33px;
            float: right; padding: 0px; margin-top: 3px; *margin-top: -38px; margin-right: 32px;
            border-top: 0px dotted #b4b4b4;">
                                    <a href="//my.vancl.com/comment/Appraisetransfer/1067444" style="width: 120px;
                height: 33px; background-color: #d46a6a; color: #fff; display: block; vertical-align: middle;
                float: right; line-height: 33px;"><span style="line-height: 33px;">我要评论</span></a>
                                </div>
                            </h2>

                            <div class="pinglunContent" style="margin-left: 34px;">
                                <div class="NewCommentDetail">

                                    <div class="pinglunTab pinglunT">

                                        <div class="pinglunTabLef fl" style="border-top: 0px dotted #b4b4b4;">

                                            <div class="pinlunCon fl">
                                                <p>
                                                    挺好，就是颜色有点不太满意</p>

                                                <span class="pinlunTime">
                            2019-10-21</span>


                                            </div>
                                            <div class="productCon fr">
                                                <span>颜色：午夜蓝</span>
                                                <span>尺码：XL

                        </span><span>身高：0cm</span> <span>体重：0.0kg</span> <span
                                                    class="plsize">
                            <尺码合适>
                        </span>
                                            </div>
                                        </div>

                                        <div class="pinglunTabRig fr" style="border-top: 0px dotted #b4b4b4;">

                                            <a class="track" name="item-item-comment-user_id" target="_blank"
                                               href="/comments/c0dpT02wsJ8X*Wxa4gdHsw==.html">
                                                Ja*** Liu</a><span
                                                class="blank10"></span>

                                            <div class="UserLevel" style="background-position: 0px -0px">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="pinglunTab pinglunT">

                                        <div class="pinglunTabLef fl">

                                            <div class="pinlunCon fl">
                                                <p>
                                                    皮肤衣的防晒透气还是不错的，设计细节可圈可点。可惜当时货品尺码不全，本来想买夜空蓝，最后买了浅蓝色，胖子还是穿深色好点…😊</p>

                                                <span class="pinlunTime">
                            2019-10-20</span>


                                            </div>
                                            <div class="productCon fr">
                                                <span>颜色：冰蓝色</span>
                                                <span>尺码：XL

                        </span><span>身高：0cm</span> <span>体重：0.0kg</span> <span
                                                    class="plsize">
                            <尺码合适>
                        </span>
                                            </div>
                                        </div>

                                        <div class="pinglunTabRig fr">

                                            <a class="track" name="item-item-comment-user_id" target="_blank"
                                               href="/comments/ILawKoHMj4iBdtqq3t6Mww==.html">
                                                *ancl_3836****</a><span
                                                class="blank10"></span>

                                            <div class="UserLevel" style="background-position: 0px -0px">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="pinglunTab pinglunT">

                                        <div class="pinglunTabLef fl">

                                            <div class="pinlunCon fl">
                                                <p>
                                                    质量很好，穿着舒服</p>

                                                <ul class="tb-thumb" style="overflow: hidden;">

                                                    <li style="float: left; margin-right: 10px; width: 50px; height: 50px;">
                                                        <a href="javascript:;">
                                                            <img class="liufengimg" picid="1"
                                                                 src="https://i1.vanclimg.com/cms/comment/20191018/e5fc5f59-646c-40f6-ae56-d421513a5a17.jpg"
                                                                 alt="https://i1.vanclimg.com/cms/comment/20191018/e5fc5f59-646c-40f6-ae56-d421513a5a17.jpg"
                                                                 mid="https://i1.vanclimg.com/cms/comment/20191018/e5fc5f59-646c-40f6-ae56-d421513a5a17.jpg"
                                                                 big="https://i1.vanclimg.com/cms/comment/20191018/e5fc5f59-646c-40f6-ae56-d421513a5a17.jpg"
                                                                 style="width: 50px; height: 50px;
                                    margin-right: 4px;"/>
                                                        </a></li>

                                                </ul>
                                                <div class="bigpic" style="display: none;">
                                                    <ul class="picPage" style="overflow: hidden; border: 1px solid #b4b4b4; position: relative;
                                width: 100%; height: 100%; *zoom: 1;">
                                                        <li dire="left" class="picSlide toleft" piccount="1"><a
                                                                href="javascript:;"></a>
                                                        </li>
                                                        <li style="height: 30px; width: 100%; margin: 0px 0px 0px 5px; line-height: 30px;
                                    text-align: left;"><a class="pichide" href="javascript:;"
                                                          style="padding-left: 10px;">
                                                            收起</a> <a class="findyunatu" href="" target="_blank"
                                                                      style="padding-left: 15px;">查看原图</a><a
                                                                class="rotateleft" href="javascript:;"
                                                                style="padding-left: 15px;">向左旋转</a>
                                                            <a class="rotateright" href="javascript:;"
                                                               style="padding-left: 15px;">向右旋转</a></li>
                                                        <li style="text-align: center;" imgw="" imgh="" imgisr=""
                                                            class="imgli"><a class="imghref"
                                                                             target="_blank" href="" style="">
                                                            <img src="" alt=""></a></li>
                                                        <li dire="right" class="picSlide toright" piccount="1"><a
                                                                href="javascript:;"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <input class="hidPicCount" type="hidden" value="1"/>

                                                <span class="pinlunTime">
                            2019-10-18</span>


                                            </div>
                                            <div class="productCon fr">
                                                <span>颜色：冰蓝色</span>
                                                <span>尺码：M

                        </span><span>身高：0cm</span> <span>体重：0.0kg</span> <span
                                                    class="plsize">
                            <尺码合适>
                        </span>
                                            </div>
                                        </div>

                                        <div class="pinglunTabRig fr">

                                            <a class="track" name="item-item-comment-user_id" target="_blank"
                                               href="/comments/ze2k0+OgMZY=.html">
                                                ****l_333874*</a><span
                                                class="blank10"></span>

                                            <div class="UserLevel" style="background-position: 0px -14px">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="pinglunTab pinglunT">

                                        <div class="pinglunTabLef fl">

                                            <div class="pinlunCon fl">
                                                <p>
                                                    还不错，质量挺好的。</p>

                                                <ul class="tb-thumb" style="overflow: hidden;">

                                                    <li style="float: left; margin-right: 10px; width: 50px; height: 50px;">
                                                        <a href="javascript:;">
                                                            <img class="liufengimg" picid="1"
                                                                 src="https://i1.vanclimg.com/cms/comment/20191015/f998a7f0-586d-469a-b5ee-c04cd1376ddb.jpg"
                                                                 alt="https://i1.vanclimg.com/cms/comment/20191015/f998a7f0-586d-469a-b5ee-c04cd1376ddb.jpg"
                                                                 mid="https://i1.vanclimg.com/cms/comment/20191015/f998a7f0-586d-469a-b5ee-c04cd1376ddb.jpg"
                                                                 big="https://i1.vanclimg.com/cms/comment/20191015/f998a7f0-586d-469a-b5ee-c04cd1376ddb.jpg"
                                                                 style="width: 50px; height: 50px;
                                    margin-right: 4px;"/>
                                                        </a></li>

                                                </ul>
                                                <div class="bigpic" style="display: none;">
                                                    <ul class="picPage" style="overflow: hidden; border: 1px solid #b4b4b4; position: relative;
                                width: 100%; height: 100%; *zoom: 1;">
                                                        <li dire="left" class="picSlide toleft" piccount="1"><a
                                                                href="javascript:;"></a>
                                                        </li>
                                                        <li style="height: 30px; width: 100%; margin: 0px 0px 0px 5px; line-height: 30px;
                                    text-align: left;"><a class="pichide" href="javascript:;"
                                                          style="padding-left: 10px;">
                                                            收起</a> <a class="findyunatu" href="" target="_blank"
                                                                      style="padding-left: 15px;">查看原图</a><a
                                                                class="rotateleft" href="javascript:;"
                                                                style="padding-left: 15px;">向左旋转</a>
                                                            <a class="rotateright" href="javascript:;"
                                                               style="padding-left: 15px;">向右旋转</a></li>
                                                        <li style="text-align: center;" imgw="" imgh="" imgisr=""
                                                            class="imgli"><a class="imghref"
                                                                             target="_blank" href="" style="">
                                                            <img src="" alt=""></a></li>
                                                        <li dire="right" class="picSlide toright" piccount="1"><a
                                                                href="javascript:;"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <input class="hidPicCount" type="hidden" value="1"/>

                                                <span class="pinlunTime">
                            2019-10-15</span>


                                            </div>
                                            <div class="productCon fr">
                                                <span>颜色：白色</span>
                                                <span>尺码：L

                        </span><span>身高：0cm</span> <span>体重：0.0kg</span> <span
                                                    class="plsize">
                            <尺码合适>
                        </span>
                                            </div>
                                        </div>

                                        <div class="pinglunTabRig fr">

                                            <a class="track" name="item-item-comment-user_id" target="_blank"
                                               href="/comments/ri3q*BDN8bPc4OexFDBKjw==.html">
                                                yul***01</a><span
                                                class="blank10"></span>

                                            <div class="UserLevel" style="background-position: 0px -14px">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="pinglunTab pinglunT">

                                        <div class="pinglunTabLef fl">

                                            <div class="pinlunCon fl">
                                                <p>
                                                    面料凉爽，不会晒黑晒伤！</p>

                                                <span class="pinlunTime">
                            2019-09-26</span>


                                            </div>
                                            <div class="productCon fr">
                                                <span>颜色：冰蓝色</span>
                                                <span>尺码：XXL

                        </span><span>身高：0cm</span> <span>体重：0.0kg</span> <span
                                                    class="plsize">
                            <尺码合适>
                        </span>
                                            </div>
                                        </div>

                                        <div class="pinglunTabRig fr">

                                            <a class="track" name="item-item-comment-user_id" target="_blank"
                                               href="/comments/TiOv0fEmhZyHYVAvcg3Y+g==.html">
                                                *ancl_7999****</a><span
                                                class="blank10"></span>

                                            <div class="UserLevel" style="background-position: 0px -14px">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="pinglunTab pinglunT">

                                        <div class="pinglunTabLef fl">

                                            <div class="pinlunCon fl">
                                                <p>
                                                    衣服非常好，第二次买了，凉爽透气
                                                </p>

                                                <ul class="tb-thumb" style="overflow: hidden;">

                                                    <li style="float: left; margin-right: 10px; width: 50px; height: 50px;">
                                                        <a href="javascript:;">
                                                            <img class="liufengimg" picid="1"
                                                                 src="https://i2.vanclimg.com/cms/comment/20190913/c73481af-c9ce-472b-b3bc-409eebac3886.jpg"
                                                                 alt="https://i2.vanclimg.com/cms/comment/20190913/c73481af-c9ce-472b-b3bc-409eebac3886.jpg"
                                                                 mid="https://i2.vanclimg.com/cms/comment/20190913/c73481af-c9ce-472b-b3bc-409eebac3886.jpg"
                                                                 big="https://i2.vanclimg.com/cms/comment/20190913/c73481af-c9ce-472b-b3bc-409eebac3886.jpg"
                                                                 style="width: 50px; height: 50px;
                                    margin-right: 4px;"/>
                                                        </a></li>

                                                </ul>
                                                <div class="bigpic" style="display: none;">
                                                    <ul class="picPage" style="overflow: hidden; border: 1px solid #b4b4b4; position: relative;
                                width: 100%; height: 100%; *zoom: 1;">
                                                        <li dire="left" class="picSlide toleft" piccount="1"><a
                                                                href="javascript:;"></a>
                                                        </li>
                                                        <li style="height: 30px; width: 100%; margin: 0px 0px 0px 5px; line-height: 30px;
                                    text-align: left;"><a class="pichide" href="javascript:;"
                                                          style="padding-left: 10px;">
                                                            收起</a> <a class="findyunatu" href="" target="_blank"
                                                                      style="padding-left: 15px;">查看原图</a><a
                                                                class="rotateleft" href="javascript:;"
                                                                style="padding-left: 15px;">向左旋转</a>
                                                            <a class="rotateright" href="javascript:;"
                                                               style="padding-left: 15px;">向右旋转</a></li>
                                                        <li style="text-align: center;" imgw="" imgh="" imgisr=""
                                                            class="imgli"><a class="imghref"
                                                                             target="_blank" href="" style="">
                                                            <img src="" alt=""></a></li>
                                                        <li dire="right" class="picSlide toright" piccount="1"><a
                                                                href="javascript:;"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <input class="hidPicCount" type="hidden" value="1"/>

                                                <span class="pinlunTime">
                            2019-09-13</span>


                                            </div>
                                            <div class="productCon fr">
                                                <span>颜色：白色</span>
                                                <span>尺码：XXL

                        </span><span>身高：0cm</span> <span>体重：0.0kg</span> <span
                                                    class="plsize">
                            <尺码合适>
                        </span>
                                            </div>
                                        </div>

                                        <div class="pinglunTabRig fr">

                                            <a class="track" name="item-item-comment-user_id" target="_blank"
                                               href="/comments/nqLE+Im3X7OXKhJjoucHdw==.html">
                                                ***cl_319436**</a><span
                                                class="blank10"></span>

                                            <div class="UserLevel" style="background-position: 0px -14px">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="pinglunTab pinglunT">

                                        <div class="pinglunTabLef fl">

                                            <div class="pinlunCon fl">
                                                <p>
                                                    非常好，买了两件，凉爽透气</p>

                                                <ul class="tb-thumb" style="overflow: hidden;">

                                                    <li style="float: left; margin-right: 10px; width: 50px; height: 50px;">
                                                        <a href="javascript:;">
                                                            <img class="liufengimg" picid="1"
                                                                 src="https://i1.vanclimg.com/cms/comment/20190913/380b34c7-e74d-4467-af83-ad3a2b2c3e82.jpg"
                                                                 alt="https://i1.vanclimg.com/cms/comment/20190913/380b34c7-e74d-4467-af83-ad3a2b2c3e82.jpg"
                                                                 mid="https://i1.vanclimg.com/cms/comment/20190913/380b34c7-e74d-4467-af83-ad3a2b2c3e82.jpg"
                                                                 big="https://i1.vanclimg.com/cms/comment/20190913/380b34c7-e74d-4467-af83-ad3a2b2c3e82.jpg"
                                                                 style="width: 50px; height: 50px;
                                    margin-right: 4px;"/>
                                                        </a></li>

                                                </ul>
                                                <div class="bigpic" style="display: none;">
                                                    <ul class="picPage" style="overflow: hidden; border: 1px solid #b4b4b4; position: relative;
                                width: 100%; height: 100%; *zoom: 1;">
                                                        <li dire="left" class="picSlide toleft" piccount="1"><a
                                                                href="javascript:;"></a>
                                                        </li>
                                                        <li style="height: 30px; width: 100%; margin: 0px 0px 0px 5px; line-height: 30px;
                                    text-align: left;"><a class="pichide" href="javascript:;"
                                                          style="padding-left: 10px;">
                                                            收起</a> <a class="findyunatu" href="" target="_blank"
                                                                      style="padding-left: 15px;">查看原图</a><a
                                                                class="rotateleft" href="javascript:;"
                                                                style="padding-left: 15px;">向左旋转</a>
                                                            <a class="rotateright" href="javascript:;"
                                                               style="padding-left: 15px;">向右旋转</a></li>
                                                        <li style="text-align: center;" imgw="" imgh="" imgisr=""
                                                            class="imgli"><a class="imghref"
                                                                             target="_blank" href="" style="">
                                                            <img src="" alt=""></a></li>
                                                        <li dire="right" class="picSlide toright" piccount="1"><a
                                                                href="javascript:;"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <input class="hidPicCount" type="hidden" value="1"/>

                                                <span class="pinlunTime">
                            2019-09-13</span>


                                            </div>
                                            <div class="productCon fr">
                                                <span>颜色：白色</span>
                                                <span>尺码：XXL

                        </span><span>身高：0cm</span> <span>体重：0.0kg</span> <span
                                                    class="plsize">
                            <尺码合适>
                        </span>
                                            </div>
                                        </div>

                                        <div class="pinglunTabRig fr">

                                            <a class="track" name="item-item-comment-user_id" target="_blank"
                                               href="/comments/nqLE+Im3X7OXKhJjoucHdw==.html">
                                                ***cl_319436**</a><span
                                                class="blank10"></span>

                                            <div class="UserLevel" style="background-position: 0px -14px">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="pinglunTab pinglunT">

                                        <div class="pinglunTabLef fl">

                                            <div class="pinlunCon fl">
                                                <p>
                                                    穿着舒服，冰感顺滑</p>

                                                <span class="pinlunTime">
                            2019-09-04</span>


                                            </div>
                                            <div class="productCon fr">
                                                <span>颜色：冰蓝色</span>
                                                <span>尺码：L

                        </span><span>身高：0cm</span> <span>体重：0.0kg</span> <span
                                                    class="plsize">
                            <尺码合适>
                        </span>
                                            </div>
                                        </div>

                                        <div class="pinglunTabRig fr">

                                            <a class="track" name="item-item-comment-user_id" target="_blank"
                                               href="/comments/ASZXvLDsu07Im85Ew6EQtA==.html">
                                                vanc*****65685</a><span
                                                class="blank10"></span>

                                            <div class="UserLevel" style="background-position: 0px -0px">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="pinglunTab pinglunT">

                                        <div class="pinglunTabLef fl">

                                            <div class="pinlunCon fl">
                                                <p>
                                                    皮肤衣挺凉快的</p>

                                                <span class="pinlunTime">
                            2019-07-31</span>


                                            </div>
                                            <div class="productCon fr">
                                                <span>颜色：冰蓝色</span>
                                                <span>尺码：M

                        </span><span>身高：0cm</span> <span>体重：0.0kg</span> <span
                                                    class="plsize">
                            <尺码合适>
                        </span>
                                            </div>
                                        </div>

                                        <div class="pinglunTabRig fr">

                                            <a class="track" name="item-item-comment-user_id" target="_blank"
                                               href="/comments/XDD4rZcvJ9B8IExjAV8L4A==.html">
                                                曲目</a><span
                                                class="blank10"></span>

                                            <div class="UserLevel" style="background-position: 0px -0px">
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div id="feedbackpagerarea">
                                <div id="styleAssessesPager" class="area2Page">


                                    <div class="area2Page">
                                        <ul>

                                            <li class="track" name="item-item-comment-page_1"><strong>1</strong></li>

                                        </ul>
                                    </div>


                                </div>
                                <div class="act0703Link02" style="display: none;">

                                </div>
                            </div>

                        </div>
                        <input id="StyleID" value="1067444" type="hidden"/>
                        <a id="anchorQuiz">&nbsp</a>

                        <script type="text/JavaScript"
                                src="//js.vanclimg.com/js.ashx?href=/item/rotate.js&compress&v=2016082902"></script>
                        <script type="text/javascript">
                            $($(".hzcTitle li").bind("click", function () {
                                $(".dpyPLkeywords a").removeClass("sd");
                            }));

                            $(document).ready(function () {

                                $(".bigpic").hide();
                                var oldbig = "0";
                                var isangle = 0; //是否旋转 0 未旋转。1 旋转；
                                var angle = 90; //旋转角度
                                var whence = false;

                                $(".rotateleft").live("click", function () {
                                    var width, height, isr, imgwidth, imgheight;
                                    width = $(this).parent().parent().find(".imgli").attr("imgw");  // 图片加载时计算得到的宽度(旋转后的宽度)
                                    height = $(this).parent().parent().find(".imgli").attr("imgh"); //图片加载时计算得到的高度(旋转后的高度度)
                                    imgwidth = $(this).parent().parent().find(".toright").attr("imgw");  // 图片加载时计算得到的宽度(原始)
                                    imgheight = $(this).parent().parent().find(".toright").attr("imgh"); //图片加载时计算得到的高度(原始)
                                    isr = $(this).parent().parent().find(".imgli").attr("imgIsr"); //图片是否进行了缩放。1 缩放 0 没有缩放
                                    if (isangle == 0) {
                                        whence = true;
                                    } else {
                                        whence = false;
                                    }
                                    if ($(this).parent().parent().parent().find(".imghref img").html() == null) {
                                        $(this).parent().parent().parent().find(".imghref canvas").rotateLeft(angle, whence, imgwidth, imgheight);
                                    } else {
                                        $(this).parent().parent().parent().find(".imghref img").rotateLeft(angle, whence, imgwidth, imgheight);
                                    }
                                    if (isr == 1) {
                                        $(this).parent().parent(".picPage").css("height", "auto");

                                        //图片旋转后交换宽高
                                        var imgwh;
                                        imgwh = width;
                                        width = height;
                                        height = imgwh;

                                        //给图片展示区UL赋值
                                        $(this).parent().parent(".picPage").css("width", width + "px");
                                        $(this).parent().parent(".picPage").css("height", height * 1 + 30 * 1 + "px");
                                        $(this).parent().parent().find(".imgli").attr("imgw", width);
                                        $(this).parent().parent().find(".imgli").attr("imgh", height);
                                    }


                                    isangle = 1;
                                });

                                $(".rotateright").live("click", function () {
                                    var width, height, isr, imgwidth, imgheight;
                                    width = $(this).parent().parent().find(".imgli").attr("imgw");  // 图片加载时计算得到的宽度(旋转后的宽度)
                                    height = $(this).parent().parent().find(".imgli").attr("imgh"); //图片加载时计算得到的高度(旋转后的高度度)
                                    imgwidth = $(this).parent().parent().find(".toright").attr("imgw");  // 图片加载时计算得到的宽度(原始)
                                    imgheight = $(this).parent().parent().find(".toright").attr("imgh"); //图片加载时计算得到的高度(原始)
                                    isr = $(this).parent().parent().find(".imgli").attr("imgIsr"); //图片是否进行了缩放。1 缩放 0 没有缩放
                                    if (isangle == 0) {
                                        whence = true;
                                    } else {
                                        whence = false;
                                    }
                                    if ($(this).parent().parent().parent().find(".imghref img").html() == null) {
                                        $(this).parent().parent().parent().find(".imghref canvas").rotateRight(angle, whence, imgwidth, imgheight);
                                    } else {
                                        $(this).parent().parent().parent().find(".imghref img").rotateRight(angle, whence, imgwidth, imgheight);
                                    }
                                    if (isr == 1) {
                                        $(this).parent().parent(".picPage").css("height", "auto");
                                        //图片旋转后交换宽高
                                        var imgwh;
                                        imgwh = width;
                                        width = height;
                                        height = imgwh;

                                        //给图片展示区UL赋值
                                        $(this).parent().parent(".picPage").css("width", width + "px");
                                        $(this).parent().parent(".picPage").css("height", height * 1 + 30 * 1 + "px");
                                        $(this).parent().parent().find(".imgli").attr("imgw", width);
                                        $(this).parent().parent().find(".imgli").attr("imgh", height);
                                    }

                                    isangle = 1;
                                });

                                $(".pinglunT .tb-thumb li a").live("click", function () {
                                    var d = new Date();
                                    if ($(this).parent().parent().parent().find(".hidPicCount").val() == 1) {
                                        $(this).parent().parent().parent().find(".picSlide").hide();
                                    } else {
                                        $(this).parent().parent().parent().find(".picSlide").show();
                                    }

                                    if (oldbig == $(this).find("img").attr("picid") || oldbig == "0") {
                                        $(this).parent().parent().parent().find("div").toggle();
                                    } else {
                                        $(this).parent().parent().parent().find("div").toggle(true);
                                    }
                                    $(this).parents("ul").find("li").removeClass("tb-selected");
                                    $(this).parents("li").addClass("tb-selected");
                                    var imgUrlMid = $(this).find("img").attr("mid"); //小图地址
                                    var imgUrlMax = $(this).find("img").attr("big"); //大图地址
                                    //复制图片地址到图片展示区域
                                    //                        $(this).parent().parent().parent().find("div li a img").attr('src', imgUrlMid);
                                    //                        $(this).parent().parent().parent().find("div li a img").attr('rel', imgUrlMax);
                                    var imgShowArea;
                                    if ($(this).parent().parent().parent().find("div li a img").html() == null) {
                                        imgShowArea = $(this).parent().parent().parent().find("div li .imghref");
                                        imgShowArea.empty();
                                        imgShowArea.html("<img src='" + imgUrlMid + "' rel='" + imgUrlMid + "'/>");
                                    } else {
                                        imgShowArea = $(this).parent().parent().parent().find("div li a img");
                                        imgShowArea.attr('src', imgUrlMid);
                                        imgShowArea.attr('rel', imgUrlMax);

                                    }
                                    if (isangle == 1) {
                                        $(this).parent().parent().parent().find("div li a img").css("filter", "none");
                                        $(this).parent().parent().parent().find("div li a img").removeAttr("angle");
                                        $(this).parent().parent().parent().find("div li a img").removeAttr("width");
                                        $(this).parent().parent().parent().find("div li a img").removeAttr("height");
                                    }
                                    isangle = 0;

                                    try {

                                        var width, heigth, isr;

                                        getImageWidth(imgUrlMax, function (r, w, h) {
                                            isr = r;
                                            width = w;
                                            heigth = h;
                                        });
                                        $(this).parent().parent().parent().find("div li a img").css("width", width + "px");
                                        $(this).parent().parent().parent().find("div li a img").css("height", heigth + "px");

                                        $(this).parent().parent().parent().find("div li").attr("imgw", width);
                                        $(this).parent().parent().parent().find("div li").attr("imgh", heigth);
                                        $(this).parent().parent().parent().find("div li").attr("imgIsr", isr);

                                        //表示不做等比缩放。
                                        if (isr == 0) {
                                            width = 382;
                                            heigth = 256;
                                        }
                                        $(this).parent().parent().parent().find(".picPage").css("width", width + "px");
                                        $(this).parent().parent().parent().find(".picPage").css("height", heigth * 1 + 30 * 1 + "px");

                                        oldbig = $(this).find(".liufengimg").attr("picid");
                                        $(this).parent().parent().parent().find(".picSlide").attr("piccount", oldbig);

                                        $(this).parent().parent().parent().find("div li .findyunatu").attr('href', imgUrlMax);
                                        $(this).parent().parent().parent().find("div li .imghref").attr('href', imgUrlMax);

                                    } catch (err) {
                                        throw err.Message;
                                    }


                                    if ($(this).parent().parent().parent().find("div").is(":hidden")) {
                                        $(this).parent().removeClass('tb-selected');
                                    }

                                    if ($(this).parent().parent().parent().find(".toleft").attr("piccount") == 1) {
                                        $(this).parent().parent().parent().find(".toleft").hide();
                                    } else {
                                        $(this).parent().parent().parent().find(".toleft").show();
                                    }

                                    if ($(this).parent().parent().parent().find(".toright").attr("piccount") == $(this).parent().parent().parent().find(".hidPicCount").val()) {
                                        $(this).parent().parent().parent().find(".toright").hide();
                                    } else {
                                        $(this).parent().parent().parent().find(".toright").show();
                                    }
                                });

                                $(".pichide").live("click", function () {
                                    $(this).parents(".bigpic").hide();
                                    $(this).parents(".bigpic").find(".picSlide").attr("piccount", "1");
                                    $(this).parents(".bigpic").parent().find(".tb-thumb li").removeClass('tb-selected');
                                });


                                $(".picSlide").live("click", function () {
                                    var direction = $(this).attr("dire");
                                    var nowPage = $(this).attr("piccount");
                                    var allPicCount = $(this).parent().parent().parent().find(".hidPicCount").val();
                                    var thisleft, imgUrlMid, imgUrlMax, width, heigth, isr;
                                    var d = new Date();
                                    nowPage = nowPage * 1;
                                    //alert(nowPage);
                                    if (direction == "left") {
                                        if (nowPage == 1) {
                                            return;
                                        } else {
                                            //alert('走left');
                                            nowPage = nowPage - 1;
                                            //thisleft = $(this).parent().parent().parent().find(".tb-thumb li a img[picid='" + nowPage + "']");
                                            $(this).parent().parent().parent().find(".tb-thumb li").removeClass("tb-selected");
                                            $(this).parent().parent().parent().find(".tb-thumb li a img[picid='" + nowPage + "']").parent().parent().addClass("tb-selected");
                                            imgUrlMid = $(this).parent().parent().parent().find(".tb-thumb li a img[picid='" + nowPage + "']").attr("mid"); //小图地址
                                            imgUrlMax = $(this).parent().parent().parent().find(".tb-thumb li a img[picid='" + nowPage + "']").attr("big"); //大图地址


                                            var imgShowArea;
                                            if ($(this).parent().find("li a img").html() == null) {
                                                imgShowArea = $(this).parent().find("li .imghref");
                                                imgShowArea.empty();
                                                imgShowArea.html("<img src='" + imgUrlMid + "' rel='" + imgUrlMid + "'/>");
                                            } else {
                                                imgShowArea = $(this).parent().find("li a img");
                                                imgShowArea.attr('src', imgUrlMid);
                                                imgShowArea.attr('rel', imgUrlMax);

                                            }
                                            if (isangle == 1) {
                                                $(this).parent().find("li a img").css("filter", "");
                                                $(this).parent().find("li a img").removeAttr("angle");
                                                $(this).parent().find("li a img").removeAttr("width");
                                                $(this).parent().find("li a img").removeAttr("height");
                                            }
                                            isangle = 0;
                                            try {
                                                //alert('try');
                                                var thisleft3 = $(this).parent().parent().parent().find(".tb-thumb .liufengimg").eq(nowPage - 1);
                                                var _picid3 = thisleft3.attr("picid");
                                                oldbig = _picid3;
                                                $(this).parent().find(".picSlide").attr("piccount", _picid3);

                                                getImageWidth(imgUrlMax, function (r, w, h) {
                                                    isr = r;
                                                    width = w;
                                                    heigth = h;
                                                });
                                                $(this).parent().find("li a img").css("width", width + "px");
                                                $(this).parent().find("li a img").css("height", heigth + "px");

                                                $(this).parent().find("li").attr("imgw", width);
                                                $(this).parent().find("li").attr("imgh", heigth);
                                                $(this).parent().find("li").attr("imgIsr", isr);

                                                if (isr == 0) {
                                                    width = 382;
                                                    heigth = 256;
                                                }

                                                $(this).parent().css("width", width + "px");
                                                $(this).parent().css("*width", width + "px");
                                                $(this).parent().css("height", (heigth * 1 + 30) + "px");

                                                $(this).parent().find(".toleft").show();
                                                $(this).parent().find(".toright").show();
                                                if ($(this).attr("piccount") == 1) {
                                                    $(this).parent().find(".toleft").hide();
                                                }

                                                if ($(this).attr("piccount") == $(this).parent().parent().parent().find(".hidPicCount").val()) {
                                                    $(this).parent().find(".toright").hide();
                                                }

                                                $(this).parent().find("li .findyunatu").attr("href", imgUrlMax);
                                                $(this).parent().find("li .imghref").attr("href", imgUrlMax);
                                            } catch (err) {
                                                throw err.Message;
                                            }

                                        }
                                    }
                                    if (direction == "right") {
                                        if (nowPage == 5) {
                                            return;
                                        } else {
                                            if (nowPage == allPicCount) {
                                                return;
                                            }
                                            nowPage = nowPage * 1 + 1 * 1;

                                            $(this).parent().parent().parent().find(".tb-thumb li").removeClass("tb-selected");
                                            $(this).parent().parent().parent().find(".tb-thumb li a img[picid='" + nowPage + "']").parent().parent().addClass("tb-selected");
                                            imgUrlMid = $(this).parent().parent().parent().find(".tb-thumb li a img[picid='" + nowPage + "']").attr("mid"); //小图地址
                                            imgUrlMax = $(this).parent().parent().parent().find(".tb-thumb li a img[picid='" + nowPage + "']").attr("big"); //大图地址

                                            var imgShowArea;
                                            if ($(this).parent().find("li a img").html() == null) {
                                                imgShowArea = $(this).parent().find("li .imghref");
                                                imgShowArea.empty();
                                                imgShowArea.html("<img src='" + imgUrlMid + "' rel='" + imgUrlMid + "'/>");
                                            } else {
                                                imgShowArea = $(this).parent().find("li a img");
                                                imgShowArea.attr('src', imgUrlMid);
                                                imgShowArea.attr('rel', imgUrlMax);

                                            }
                                            if (isangle == 1) {
                                                $(this).parent().find("li a img").css("filter", "");
                                                $(this).parent().find("li a img").removeAttr("angle");
                                                $(this).parent().find("li a img").removeAttr("width");
                                                $(this).parent().find("li a img").removeAttr("height");
                                            }
                                            isangle = 0;
                                            try {
                                                var thisleft2 = $(this).parent().parent().parent().find(".tb-thumb .liufengimg").eq(nowPage - 1);
                                                var _picid2 = thisleft2.attr("picid");
                                                oldbig = _picid2;
                                                $(this).parent().find(".picSlide").attr("piccount", _picid2);

                                                getImageWidth(imgUrlMax, function (r, w, h) {
                                                    isr = r;
                                                    width = w;
                                                    heigth = h;
                                                });
                                                $(this).parent().find("li a img").css("width", width + "px");
                                                $(this).parent().find("li a img").css("height", heigth + "px");

                                                $(this).parent().find("li").attr("imgw", width);
                                                $(this).parent().find("li").attr("imgh", heigth);
                                                $(this).parent().find("li").attr("imgIsr", isr);

                                                if (isr == 0) {
                                                    width = 382;
                                                    heigth = 256;
                                                }

                                                $(this).parent().css("width", width + "px");
                                                $(this).parent().css("height", (heigth * 1 + 30) + "px");

                                                $(this).parent().find(".toleft").show();
                                                $(this).parent().find(".toright").show();
                                                if ($(this).attr("piccount") == 1) {
                                                    $(this).parent().find(".toleft").hide();
                                                }

                                                if ($(this).attr("piccount") == $(this).parent().parent().parent().find(".hidPicCount").val()) {
                                                    $(this).parent().find(".toright").hide();
                                                }

                                                $(this).parent().find("li .findyunatu").attr("href", imgUrlMax);
                                                $(this).parent().find("li .imghref").attr("href", imgUrlMax);
                                            } catch (err) {
                                                throw err.Message;
                                            }
                                        }
                                    }
                                });
                                $("input[name=ChoosePingLun]").change(function () {
                                    var chooseval = $("input[name=ChoosePingLun]:checked").val();
                                    var mydate = new Date().getTime();
                                    var total = $(this).attr("plCount");
                                    $("#hidPlCount").val($(this).attr("plCount"));
                                    Url = "/styles/AjaxStyleAssesses.aspx?styleId=" + $("#StyleID").val() + "&pageindex=1&type=0&ispic=" + chooseval + "&version=" + mydate;
                                    loadTupianStyleAssess(1, this, Url, total);
                                });
                            });

                            function loadTupianStyleAssess(pageindex, obj, url, total) {
                                window.location.hash = "anchorPinglun";
                                window.location = window.location;
                                $(".NewCommentDetail").html("<div style='text-align:center; margin-top:20px;'><img src=\"//i.vanclimg.com/logo/loading.gif\" /></div>");
                                url = url + "&total=" + total;
                                $.get(url, function (data) {
                                    if ($.trim($("#comments", data).html()) == "") {
                                        data = "<div style='text-align:center;padding-top:10px;color:#999;padding-bottom: 10px;'>还没有最新的此类评论</div>";
                                        $(".NewCommentDetail").html(data);
                                        $("#feedbackpagerarea").hide();
                                        $(".pinglunContent").css("border-bottom", "0px");
                                        return;
                                    }
                                    $(".NewCommentDetail").empty();
                                    $(".NewCommentDetail").html($("#comments", data).html());
                                    $("#feedbackpagerarea").empty();
                                    $("#feedbackpagerarea").html($("#page", data).html());
                                    $("#feedbackpagerarea").show();
                                    $(".pinglunContent").css("border-bottom", "");
                                });
                            }

                            function getImageWidth(url, callback) {
                                var img = new Image();
                                var imgwidth, imgheigth, showMaxw, showMinw, showMaxh, showMinh, showNoww, showNowh,
                                    imgnarrow;
                                img.src = url;
                                imgwidth = img.width; //图片原始宽度
                                imgheight = img.height; //图片原始高度
                                showMaxw = 450;
                                showMinw = 382;
                                showMaxh = 400;
                                showMinh = 256;
                                if (imgwidth >= imgheight) { //横向图
                                    if (imgwidth >= showMaxw) {//如果图片宽度超出450
                                        imgnarrow = 1;
                                        var maxh = (showMaxw * imgheight) / imgwidth; //通过宽度计算高度等比缩放
                                        showNoww = showMaxw;
                                        if (maxh >= showMinh) {  //缩放后图片高度超过320
                                            showNowh = maxh;
                                        } else { //缩放后图片高度小于320
                                            if (imgheight >= showMinh) { //判断原图高度超过320
                                                showNowh = maxh
                                            } else {//不超过320
                                                //不做等比例缩放
                                                imgnarrow = 0;
                                                showNoww = imgwidth;
                                                showNowh = imgheight;
                                            }
                                        }
                                    } else { //如果图片宽度不超出450 直接使用原图高宽
                                        imgnarrow = 0;
                                        showNoww = imgwidth;
                                        showNowh = imgheight;
                                        if (imgwidth >= showMinw) {
                                            imgnarrow = 1;
                                        }


                                    }
                                } else { //竖向图
                                    if (imgheight >= showMaxh) {//如果图片高度超过450
                                        imgnarrow = 1;
                                        var w = (showMaxh * imgwidth) / imgheight; //通过高度计算宽度等比缩放
                                        showNowh = showMaxh;
                                        if (w >= showMinw) { //缩放后图片宽度超过320
                                            showNoww = w;
                                        } else {             //缩放后图片宽度不超过320
                                            if (imgwidth >= showMinw) { //判断原图宽度超过320
                                                showNoww = w;
                                            } else {//不超过320
                                                //不做等比例缩放
                                                imgnarrow = 0;
                                                showNoww = imgwidth;
                                                showNowh = imgheight;
                                            }
                                        }
                                    } else {
                                        //不做等比例缩放
                                        imgnarrow = 0;
                                        showNoww = imgwidth;
                                        showNowh = imgheight;
                                        if (imgheight >= showMinh) {
                                            imgnarrow = 1;
                                        }
                                    }
                                }
                                callback(imgnarrow, showNoww, showNowh);
                            }
                        </script>

                    </div>
                    <div class="productQuiz fr">

                        <span class="blank20"></span>
                        <div class="area3 root" name="0">

                            <h2 class="hotTitle" style="position: static; top: 0px; left: 0px; height: 40px;
        line-height: 40px; padding: 0px 0px 0px 25px; border-bottom: 1px solid #b4b4b4;">
                                <span>最新提问</span> <span class="zuixinpinglunnum">(共<strong
                                    id="quizcount">0</strong>条)</span>
                                <div class="pinglunTabRig fr" style="line-height: 40px; width: 120px; height: 33px;
            float: right; padding: 0px; margin-top: 3px; *margin-top:-38px; margin-right: 32px;  border-top: 0px dotted #b4b4b4;">
            <span id="tiwen"><a name="item-item-quiz-mine" href="#1" style="width: 120px; height: 33px;
                background-color: #d46a6a; color: #fff; display: block; vertical-align: middle;
                float: right; line-height: 33px;"><span style="line-height:33px;">我要提问</span></a> <a href="#quiz"
                                                                                                     name="quiz">
                </a></span>
                                </div>
                            </h2>
                            <div class="area3Head" style="display: none;">
        <span style="float: left; display: none;" class="area3Sp01">
            <div class="sprites" style="width: 14px; height: 14px; background-position: -3482px 0;
                margin: 6px;">
            </div>
        </span><span class="zuixinpingluntitle">最新提问</span> <span class="zuixinpinglunnum">(共<strong
                                    id="quizcount">0</strong>条)</span> <span id="tiwen"><a
                                    class="danpinBtnStyle track" name="item-item-quiz-mine" href="#1"><span>我要提问</span></a>
                <a href="#quiz" name="quiz"></a></span>

                            </div>

                            <div id="stylenoquestion">
                                <div class="area3Con">
                                    <div class="areaquestion" style="border:0px solid;">
                                        <div style="text-align: center; padding: 50px 0px 30px 0px;">
                                            暂无最新提问
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="act0703Link04" style="display: none;">
                                <a href="//review.vancl.com/quiz/1067444.html" target="_blank">查看全部问答&gt;&gt;</a></div>

                        </div>
                        <span class="blank10"></span><a name="1"></a>
                        <div class="Askquestion" style="display: none;">
                            <h3>
                                <p>
                                    <span></span>提问：刺客信条:奥德赛</p>
                            </h3>
                            <input type="hidden" id="title" value="刺客信条:奥德赛"/>
                            <dl class="questionStyle">
                                <dt>提问类型：</dt>
                                <dd>
                                    <ul id="quiztype">
                                        <li>
                                            <input type="radio" value="0" name="questionType"/><span>商品提问</span></li>
                                        <li>
                                            <input type="radio" value="1" name="questionType"/><span>促销活动提问</span></li>
                                        <li>
                                            <input type="radio" value="2" name="questionType"/><span>库存及物流提问</span></li>
                                        <li>
                                            <input type="radio" value="3" name="questionType"/><span>售后提问</span></li>
                                    </ul>
                                    <p style="display: none" id="qtype">
                                        <strong></strong><span>请选择问题分类</span></p>
                                </dd>
                                <span class="blank0"></span>
                            </dl>
                            <dl>
                                <dt>提问内容：</dt>
                                <dd>
                                    <textarea class="Askcontent" id="content"></textarea>
                                    <input type="button" class="askSubBtn" id="tijiao"/>
                                </dd>
                                <span class="blank0"></span>
                            </dl>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
</div>

<div id="bigView" style="display: none; z-index: 10">
    <img/></div>
<div style="display: none;" id="loading">
</div>
<div class="collectbox" id="collectbox" style="display: none">
    <div class="cltboxcon">
        <div id="collectboxloaging">
        </div>
        <div class="collectcontent">
            <div class="msgcltdetail">
                <h2 class="msgboxhead2" style="float: right; width: 40px;">
                    <a href="#" class="track close" name="item-close-cart"><span class="sprites" style="width: 11px;
                        height: 11px; background-position: -519px 0; margin-top: 5px; cursor: pointer"></span>
                        <span>关闭</span></a></h2>
                <div class="CltTitleDiv">
                    <div class="sprites" style="width: 59px; height: 33px; background-position: -945px 0;
                        float: left; margin-top: 10px;">
                    </div>
                    <span class="msgCltTitle">该商品已成功放入收藏夹&nbsp;<a href="//my.vancl.com/favorite" target="_blank">查看收藏夹&gt;&gt;</a></span>
                </div>
                <div class="tiinfof">
                    您还可以通过<span><a href="#" class="addmark">添加标签</a></span>为商品分类，或添加邮箱订制<span><a href="#"
                                                                                                 class="addsubscribe">余量通知</a></span>
                </div>
                <div class="infoCON">
                    <ul>
                        <li>
                            <div class="inCti addmark">
                                <span class="inCti_img02"></span>收藏标签
                            </div>
                            <div class="fonttext">
                                <span class="blank10"></span>标 签：<input type="text" class="icte" id="bq"
                                                                        maxlength="24"/><span>标签最多3个，用“，”隔开</span><br>
                                <div class="tishi">
                                    <span></span>请输入中英文、数字或下划线！
                                </div>
                                <div id="commonmarks" style="margin-top: 6px">
                                    常用标签：<label><input type="checkbox" class="input">日韩女装</label>
                                    <label>
                                        <input type="checkbox" class="input">裤子</label>
                                    <label>
                                        <input type="checkbox" class="input">牛仔裤</label></div>
                                <br>
                                <div class="button">
                                    <input type="submit" value="确定" class="submitdiv"><a href="#" class="addmark">取消</a>
                                    <div
                                            class="baocun">
                                        保存成功！
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="inCti addsubscribe">
                                <span class="inCti_img02"></span>余量通知
                            </div>
                            <div class="fonttext">
                                <span class="blank10"></span>邮 箱：<input type="text" class="icte" id="email"/><br>
                                <div class="tishi">
                                </div>
                                <br>
                                <div class="button">
                                    <input type="submit" value="确定" class="submitdiv"><a href="#" class="addsubscribe">取消</a>
                                    <div
                                            class="baocun">
                                        保存成功！
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <span class="blank5"></span>
            </div>
            <div class="gmlist">
            </div>
        </div>
        <span class="blank5"></span>
    </div>
    <div class="rightborder2">
    </div>
    <span class="blank0"></span>
    <div class="bottomborder">
    </div>
</div>
<div id="surveyID" style="width: 40px; padding: 0; position: fixed; bottom: 110px;
    _position: absolute; _top: expression(documentElement.scrollTop + 200 + 'px');
    right: 0; border: 1px #CCC; border-right: none; border-radius: 4px; -moz-border-radius: 4px;
    -webkit-border-radius: 4px; border-right-radius: none; -moz-border-radius-bottomright: 0;
    -moz-border-radius-topright: 0; text-align: center; background-color: white;
    z-index: 999; display: none;">

    <a target="_blank" href="//catalog.vancl.com/dzzz/magazine_091207.html">
        <div class="sprites spritesDg">
        </div>
    </a>
</div>
<style type="text/css">
    .CarBox_MainDragDrop {
        cursor: pointer;
    }
</style>
<div class="CarBox_Main CarBox_MainDragDrop" id="carbox" style="display: none">
    <div id="carboxloading">
    </div>
    <div style="display: none">
        <h2 class="CarBox_Title">
            <span>商品成功放入购物车</span><span class="close1 fr"></span>
        </h2>
        <span class="blank20"></span>
        <div class="CarBox_Content">
            <div class="CarBox_bg">
            </div>
            <div class="CarBox_NumPrice">
                <span class="line01 fl"></span>
                <div class="fl" style="padding-top: 14px;">
                    <p>
                        <span>共有</span><span id="shopcarcount">100</span><span>件商品</span>
                    </p>
                    <p>
                        <span class="CarBox_TotialPrice"><span>总计</span><span id="shopcarprice">￥10000.00</span>
                        </span>
                    </p>
                </div>
            </div>
            <div class="CarBox_Btn">
                <div class="CarBox_goCar">
                    <a class="track" name="item-Settlement" href="http://shopping.vancl.com/mycart" rel="nofollow"
                       target="_parent">去购物车&gt;&gt;</a></div>
                <div id="continueshop" class="CarBox_nowCheckout CarBox_nowContinueBuy jxgwbtn">
                    <a href="#" class="track" name="item-continueshop">&lt;&lt;继续购物</a></div>
            </div>
            <span class="blank5"></span>
        </div>
        <div class="CarBox_ProductList">
        </div>
    </div>
</div>
<div class="CarBox_Main" id="customizeurl" style="display: none; width: 955px; height: 633px;
    overflow: hidden">
    <div>
        <h2 class="CarBox_Title" style="width: 955px;">
            <span>我要定制</span><a href="javascript:void(0)" name="item-close-customize"
                                class="track close"><em></em>关闭</a></h2>
        <iframe style="width: 955px; height: 630px; border: 0px none; overflow: hidden" scrolling="no"
                id="customizeurlsrc"></iframe>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        //绑定拖动元素对象
        bindDrag(document.getElementById('carbox'));
    });

    function bindDrag(el) {
        //初始化参数
        var els = el.style,
            //鼠标的 X 和 Y 轴坐标
            x = y = 0;

        //邪恶的食指
        $(el).mousedown(function (e) {
            var isfixed = checkClassName(document.getElementById("carbox").className);
            document.getElementById("carbox").className = "CarBox_Main CarBox_MainDragDrop";
            //按下元素后，计算当前鼠标位置
            x = e.clientX - el.offsetLeft;
            y = e.clientY - el.offsetTop;
            //IE下捕捉焦点
            el.setCapture && el.setCapture();
            els.top = e.clientY - y + 'px';
            els.left = e.clientX - x + 'px';


            //绑定事件
            $(document).bind('mousemove', mouseMove).bind('mouseup', mouseUp);
        });

        //移动事件
        function mouseMove(e) {
            //宇宙超级无敌运算中...
            els.top = e.clientY - y + 'px';
            els.left = e.clientX - x + 'px';
            //            var top = e.clientY - y + 'px';
            //            var left = e.clientX - x + 'px';
            //            if (isFixed) {
            //                $(".CarBox_MainFixed").css("top", top);
            //                $(".CarBox_MainFixed").css("left", left);
            //            }
            //            else {
            //                $(".CarBox_Main").css("top", top);
            //                $(".CarBox_Main").css("left", left);
            //            }
            ////            els.css("top", e.clientY - y + 'px');
            ////            els.css("left", e.clientX - x + 'px');
        }

        //停止事件
        function mouseUp() {
            isFixed = false;
            //IE下释放焦点
            el.releaseCapture && el.releaseCapture();
            //卸载事件
            $(document).unbind('mousemove', mouseMove).unbind('mouseup', mouseUp);
        }

        function checkClassName(name) {
            var nameArr = name.split(' ');
            for (var i = 0; i < nameArr.length; i++) {
                if (nameArr[i] == "CarBox_MainFixed") {
                    return true;
                }
            }
            return false;
        }
    }
</script>


<input type="hidden" value="" id="recommendTimeStamp"/>
<script type="text/javascript">
    $(function () {
        var url = "http://recom-s.vancl.com/product/GetCurrentRecommendProducts";
        $.ajax({
            type: "get",
            async: false,
            url: url,
            cache: false,
            dataType: "jsonp",
            jsonp: "callback",
            success: function (data) {
                var pImg, pImgArr, pImgUrl, pName, pPrice, pUrl, pCode, strHtml = "", forIndex;
                if (data.length > 5) {
                    forIndex = 5;
                } else {
                    forIndex = data.length;
                }
                for (var i = 0; i < forIndex; i++) {
                    pName = data[i].ProductName;
                    pCode = data[i].ProductCode;
                    pPrice = data[i].SPrice;
                    pUrl = data[i].ItemUrl;
                    pImgArr = data[i].Photos;
                    if (pImgArr.length > 1) {
                        for (var j = 0; j < pImgArr.length; j++) {
                            if (pImgArr[j].IsMain) {
                                pImg = pImgArr[j].FileName;
                            }
                        }
                    } else {
                        pImg = pImgArr[0].FileName;
                    }
                    pImgUrl = "http://p5.vanclimg.com/product/";
                    if (pCode.length > 3) {
                        pImgUrl += pCode.substring(0, 1) + "/" + pCode.substring(1, 2) + "/" + pCode.substring(2, 3) + "/";
                    }
                    pImgUrl = pImgUrl + pCode + "/mid/" + pImg;
                    strHtml += "<li><a href='" + pUrl + "' target='_blank'><img src='" + pImgUrl + "'alt='' title='" + pName + "'></a><h3><a href='" + pUrl + "' target='_blank'>" + pName + "</a></h3><p><em>售价￥" + pPrice + "</em></p></li>";
                    $("#hotProduct ul").empty();
                    $("#hotProduct ul").html(strHtml);
                }
            },
            error: function (e) {
            }
        });
    });
    $(document).ready(function () {

        //            $(".onlyPageX ul li span").each(function () {
        //                $(this).text($(this).find("em").text());
        //            });
        var danpinRight = $('#danpinRight');
        var ie = window.ActiveXObject && !window.XMLHttpRequest;
        var opened = false;

        function setdanpinrighttop() {
            if (ie) {
                $("#danpinRight").css("top", document.documentElement.scrollTop + "px");
            } else {
                $("#danpinRight").css("top", 0);
            }
        }

        $(".ProductSubnav li a").bind("click", function () {
            $(".ProductSubnav li a").removeClass("selected");
            $(this).addClass("selected");
        });
        $(window).bind("scroll", function () {
            var flag = danpinRight.hasClass("danpinRightFixed");
            var divtop = 630;   //没有单品预售描述使用Top
            //var divtop = 1000;  // 添加单品预售描述后使用Top
            var doctop = $(document).scrollTop();
            //                //描述
            //                var description = $(".RsetTabCon").offset().top;
            //                //评论
            //                var access = $(".ptPinglunRig").offset().top;
            //                //问答
            //                var qa = $(".area3 ").offset().top;
            //                if (doctop > qa - 120) {
            //                    $(".ProductSubnav li a").removeClass("selected");
            //                    $(".ProductSubnav li a").eq(2).addClass("selected");
            //                }
            //                else if (doctop > access - 170) {
            //                    $(".ProductSubnav li a").removeClass("selected");
            //                    $(".ProductSubnav li a").eq(1).addClass("selected")
            //                }
            //                else if (doctop > description - 100) {
            //                    $(".ProductSubnav li a").removeClass("selected");
            //                    $(".ProductSubnav li a").eq(0).addClass("selected")
            //                }
            if (doctop > divtop + 42) {
                $("#ProductTitleHide").addClass("hideAddtoCar");
                $("#ProductTitleHide").show();
                setdanpinrighttop();
                if (opened) {
                    danpinRight.show();
                } else {
                    danpinRight.hide();
                }
                //                    if (ie) {
                //                        alert($("#danpinRight").css("top"));
                //                    }
            } else {
                $("#ProductTitleHide").hide();
                $("#inputCarHide").hide();
                $("#inputCarSuccess").hide();
                $("#danpinRight").removeClass("danpinRightFixed");
                $("#danpinRight").css("top", 0);
                danpinRight.show();
                opened = false;
            }
        });


        $("#hidCartab").click(function () {
            danpinRight.addClass("danpinRightFixed");
            danpinRight.show();
            setdanpinrighttop();
            opened = true;
        });
        $(".close1").click(function () {
            $("#danpinRight").removeClass("danpinRightFixed");
            $("#danpinRight").css("top", 0);
        });
        $("#catbtnHide").click(function () {
            $("#inputCarSuccess").show();
        });

        $("#addToShoppingCar").live("click", function () {
            if (danpinRight != null || danpinRight != undefined) {
                var btn = danpinRight.hasClass("danpinRightFixed");

                if (btn) {
                    $("#carbox").removeAttr("top");
                } else {
                }
            }
        });

        $("#lijidingzhi").live("click", function () {
            var pQty, pSku, pName, pRef, pPrice, pSize, pCode, pStyleid, pShopid, param, imgurl, url, rdate;
            rdate = new Date().getTime();
            ajaxIsLogined(function () {
                if (!isLogined) {
                    $.fn.alert('此操作需要登录，您现在要登录吗？', function (b) {
                        if (b)
                            location.href = 'https://login.vancl.com/login/login.aspx?' + location.href;
                    }, {type: 2});
                } else {
                    pQty = $("#selectedAmount").val();
                    pSku = getCloseCode();
                    pName = $.trim($("#productTitle h2").text().replace($("#productTitle h2 span").text(), ""));
                    pRef = $("#hidref").val(); //$("#lijidingzhi").attr("ref") == "#" ? "" : $("#lijidingzhi").attr("ref");
                    pPrice = $("#pricearea .cuxiaoPrice strong:eq(0)").text();
                    pSize = $.trim($(".selSize #onlickSelSize p").text());
                    pCode = $("#productcode").text().split("：")[1];
                    pStyleid = $("#styleinfo").attr("name");
                    pShopid = $("#shopid").val();
                    imgurl = $("#imageMenu .SpriteSmallImgs").eq(0).attr("name").replace("small", "mid");
                    param = pQty + "," + pSku + "," + pName + "," + pRef + "," + pPrice + "," + pSize + "," + pCode + "," + pStyleid + "," + pShopid + "," + imgurl;
                    url = "styles/dingzhi.aspx?productInfo=" + encodeURI(param) + "&v=" + rdate;
                    $.get(url, function (data) {
                        if (data == "nologin") {
                            $.fn.alert('此操作未登录，您现在要登录吗？', function (b) {
                                if (b)
                                    location.href = 'https://login.vancl.com/login/login.aspx?' + location.href;
                            }, {type: 2});
                        } else if (data == "suc") {
                            var hostname = window.location.host;
                            var demoname = hostname.indexOf("demo");
                            var returnSiteUrl = "http://customization.vancl.com/personal";
                            if (demoname > -1) {
                                returnSiteUrl = "http://democustomization.vancl.com/personal";
                            }
                            window.location.href = returnSiteUrl; //"http://democustomization.vancl.com";
                        }
                    });
                }
            });
        });
    });
</script>
<script type="text/javascript">

    var IO = document.getElementById('ProductTitleHide'), Y = IO, H = 0, IE6;
    IE6 = window.ActiveXObject && !window.XMLHttpRequest;
    while (Y) {
        H += Y.offsetTop;
        Y = Y.offsetParent
    }
    ;
    if (IE6) {
        IO.style.cssText = "top:expression(eval(document.documentElement.scrollTop));display:none;";
    }
    //        $(window).bind("scroll", function () {
    //            var d = document, s = Math.max(d.documentElement.scrollTop, document.body.scrollTop);
    //            if (s > H && IO.fix || s <= H && !IO.fix) return;
    //            if (!IE6) { IO.style.position = IO.fix ? "" : "fixed"; }
    //            IO.fix = !IO.fix;

    //        });
</script>


<span style="height: 30px"></span>
<div class="blank0" style="height:32px;"></div>
<div class="vanclFoot" style="margin-top:0px;">
    <div class="vanclCustomer publicfooterclear">
        <ul>
            <li><a href="//help.vancl.com/Home/contact" rel="nofollow" target="_blank"><p class="onlineCustomer"><img
                    src="//i4.vanclimg.com/cms/20160802/onlinecustomer.png" style="width:auto;height:auto;"></p>
                <p class="onlineTime"> 7X9小时在线客服</p></a></li>
            <li><a href="//help.vancl.com/Category/43-1.html" rel="nofollow" target="_blank"><p class="seven"><img
                    src="//i2.vanclimg.com/cms/20160802/seven.png" style="width:auto;height:auto;"></p>
                <p> 7天内退换货</p>
                <p> 购物满199元免运费</p></a></li>
            <li class="twocode"><p><img src="//i7.m.vancl.com/oms/2014_8_29_16_39_33_7709.jpg"
                                        style="width: 104px; height: 104px;"></p>
                <p> 扫描下载<em>凡客</em>客户端</p></li>
        </ul>
    </div>
    <div class="vanclOthers publicfooterclear">
        <ul>
            <li><a href="//www.vancl.com/help/about.htm" target="_blank">关于凡客</a></li>
            <li><a href="//help.vancl.com/Content/1.html" target="_blank">新手指南</a></li>
            <li><a href="//help.vancl.com/category/transfer" target="_blank">配送范围及时间</a></li>
            <li><a href="//help.vancl.com/Category/31-1.html" target="_blank">支付方式</a></li>
            <li><a href="//help.vancl.com/Category/43-1.html" target="_blank">售后服务</a></li>
            <li class="none"><a href="//help.vancl.com/" target="_blank">帮助中心</a></li>
        </ul>
    </div>
</div>
<div id="footerArea" class="">
    <div class="footBottom">
        <div class="footBottomTab"><p> Copyright 2007 - 2019 vancl.com All Rights Reserved 京ICP证100557号
            京公网安备11011502002400号 出版物经营许可证新出发京批字第直110138号</p>
            <p> 凡客诚品（北京）科技有限公司</p></div>
    </div>
    <span class="blank20"></span>
    <div class="subFooter"><a rel="nofollow" href="https://search.szfw.org/cert/l/CX20111229001302001330"
                              class="redLogo" target="_blank"></a><span class="costumeOrg"></span><a rel="nofollow"
                                                                                                     href="//www.315online.com.cn/member/315090053.html"
                                                                                                     class="wsjyBzzx"
                                                                                                     target="_blank"></a><a
            rel="nofollow" href="//www.hd315.gov.cn/beian/view.asp?bianhao=010202010081900017" class="vanclMsg"
            target="_blank"></a><a class="vanclqingNian" target="_blank" href="//www.vancl.com/help/about_2.htm"
                                   rel="nofollow"></a><a href="//trust.cctvmall.cn/reg/templates/brandtem1010.html"
                                                         style="display: inline-block;" target="_blank"><img
            style="width: 120px; height: 39px;" src="//i.vanclimg.com/other/kexin_brand_for_others.png"/></a>
        <div class="blank0"></div>
    </div>
</div>
<div class="BayWindow" style="position: fixed; right: 10px; bottom: 20px; z-index:10">
    <ul>
        <li class="newPeople"><a href="//catalog.vancl.com/getticket.html" target="_blank"></a></li>
        <li class="downApp"><a href="//m.vancl.com/html/app.html#ref=hp-hp-head-mobile-v:n" target="_blank"></a></li>
        <li class="online"><a href="//imweb.vancl.com/" target="_blank"></a></li>
        <li class="BlackTop"><a href="javascript:;"
                                onclick="javascript:document.getElementById('headerTopArea').scrollIntoView()"></a></li>
    </ul>
</div>
<script type="text/javascript"> $(document).ready(function () {
    var hostname = window.location.host;
    if (hostname != "demowww.vancl.com" && hostname != "www.vancl.com" && hostname != "www.fanke.com") {
        $(".subnavCon em").css({display: "block"});
        $(".subnavCon").hover(function () {
            $(this).find("em").addClass("emtop");
            $(this).parent().find(".categoryList").show();
        }, function () {
            $(this).find("em").removeClass("emtop");
            $(this).parent().find(".categoryList").hide();
        });
        $(".categoryList").hover(function () {
            $(this).show();
        }, function () {
            $(this).hide();
            $(this).parent().find(".subnavCon em").removeClass("emtop");
        });
    }
});
setTimeout(function () {
    var bp = document.createElement('script');
    bp.src = '//push.zhanzhang.baidu.com/push.js';
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(bp, s);
}, 1000);
$(function () {
    $(".subNav").hide();
    $(".navlist ul li").hover(function () {
        $(this).find(".subNav").stop(true, true);
        $(this).find(".subNav").slideDown();
    }, function () {
        $(this).find(".subNav").stop(true, true);
        $(this).find(".subNav").slideUp();
    });
})</script>


<script type="text/JavaScript"
        src="//jsy.vanclimg.com/js.ashx?href=/item/public.js,/public/union_websource.js,/public/ref.js,/public/ct.js,/public/generateimagedomain.js,/vjiaAd/item.js&compress&v="></script>
<script type="text/JavaScript"
        src="//jsy.vanclimg.com/js.ashx?href=/search/jquery.autocomplete.js,/public/trace.js&compress&v="></script>
<script src="http://jsx.vanclimg.com/js.ashx?href=[item/video-js/video.min.js]" type="text/javascript"></script>
<script type="text/JavaScript">
    var player = null;
    $(".selColorArea .selColor li[name]").live("click", function () {
        if (player != null) {
            player.dispose();
            player = null;
        }
    });
    var btn = videojs.getComponent('CloseButton');
    var MyButton = videojs.extend(btn, {
        constructor: function () {
            btn.apply(this, arguments);
        },
        handleClick: function () {
            if (player.isFullscreen()) {
                player.exitFullscreen();
                return;
            }
            player.pause();
            $('#my-video').hide();
        }
    });
    videojs.registerComponent('MyButton', MyButton);

    $('#btnShowProductVideo').live("click", function () {
        if (player != null) {
            $('#my-video').show();
            player.play();
        } else {
            var options = {};
            player = videojs('my-video', options, function onPlayerReady() {
                this.addChild('MyButton', {});
                $('#my-video').show();
                player.play();
                // How about an event listener?
                this.on('ended', function () {
                    $('#my-video').hide();
                });
            });
        }
    });
</script>

</body>
</html>
