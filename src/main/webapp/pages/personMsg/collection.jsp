<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="../../css/PersonMsg/collection.css"/>
<div class="ordercontainer">
    <h3><span>我的收藏</span></h3>
    <div class="wtitle">
        <div id='cover'></div>
        <input type="hidden" value="all" id="dingzhiType" name="dingzhiType" />
        <div id="myTab" class="wtitle_l">
            <a href="" class="track wtitle_lNa" name="my_Index_id_allOrder">全部商品<span></span></a>
        </div>
        <div id="effect" class="wtitle_l">
            <a href="javascript:void(0)" class="track" name="">已失效<span></span></a>
        </div>
        <div class="collection_search">
            <input type="search" name="" id="" size="15" value="商品搜索"/><a href="#"><img src="../../img/public/search.png" height="100%"/> </a>
        </div>
        <div>
            <a href="#" class="collection_manage">批量管理</a>
        </div>
    </div>
    <div class="blank10"></div>
    <div class="myshop">
        <table class="shoptable">

        </table>
    </div>
</div>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../../js/personMsg/collection.js"></script>