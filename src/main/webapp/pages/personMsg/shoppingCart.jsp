<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="../../css/PersonMsg/shoppingCart.css" />
<div class="ordercontainer">
    <h3><span>我的购物车</span></h3>
    <div class="wtitle">
        <div id='cover'></div>
        <input type="hidden" value="all" id="dingzhiType" name="dingzhiType" />
        <div id="myTab" class="wtitle_l">
            <a href="" class="track wtitle_lNa" name="my_Index_id_allOrder">全部商品<span></span></a>
        </div>
    </div>
    <div class="blank10"></div>
    <div class="myshop">
        <table class="shoptable">
            <tr>
                <td>
                    <input type="checkbox" class="chooseall" />&nbsp;&nbsp;全选
                </td>
                <td >商品信息</td>
                <td></td>
                <td>单价</td>
                <td>数量</td>
                <td>金额</td>
                <td>操作</td>
            </tr>
            <tr class="blank10"></tr>
        </table>
    </div>
</div>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../../js/personMsg/shoppingCart.js"></script>