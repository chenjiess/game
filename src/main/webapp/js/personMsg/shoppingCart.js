$(window.parent.document).find("#iframe").load(function () {
    var main = $(window.parent.document).find("#iframe");
    var thisheight = $(document).height() + 100;
    main.height(thisheight);
})

function getUrlParam(key) {
    // 获取参数
    var url = window.location.search;
    // 正则筛选地址栏
    var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
    // 匹配目标参数
    var result = url.substr(1).match(reg);
    //返回参数值
    return result ? decodeURIComponent(result[2]) : null;
}

var id = getUrlParam("id");
$.getJSON("../../user/queryShoppingCart", {"id": id}, function (data) {
    var $shoptable = $(".shoptable");
    var add = "";
    $(data).each(function (i) {
        add = add + "<tr><td><input type='checkbox' class='choose'></td><td><img src='" + this.gameNote.picture1 + "' width='75px' height='100px'/></td>"
            + "<td>" + this.gameNote.name + "</td><td><span>￥&nbsp;</span><em id='evenPrice" + (i + 1) + "'>" + this.gameNote.price.toFixed(2) + "</em></td>"
            + "<td><a href='javascript:void(0)' class='btn btn_minus' role='button' title='减少'></a>" +
            "<input class='inputNum' id='num" + (i + 1) + "' value='" + this.amount + "' size='1'/>" +
            "<a href='javascript:void(0)' class='btn btn_plus' role='button' title='增加'></a></td>"
            + "<td><span>￥&nbsp;</span><input type='text' id='totalPrice" + (i + 1) + "' value='" + (this.gameNote.price * this.amount).toFixed(2) + "' size='3' disabled='disabled' /></td>"
            + "<td><a href='javascript:void(0)'>删除</a>&nbsp;&nbsp;<a href='javascript:void(0)'>加入收藏</a></td></tr>"
            + "<tr class='blank10'></tr>";
    })
    $shoptable.append(add + "<tr><td colspan='7'><a href='javascript:void(0)' style='float: right'>总金额：<span><em id='totalPrice'>0</em>元</span><img src='../../img/public/commit.jpg'></a></td></tr>");
})

$(document).on("click", ".btn_minus", function () {
    var index = $(".btn_minus").index(this) + 1
    if ($("#num" + index).val() > 1) {
        var price = parseFloat($("#evenPrice" + index).html());
        $("#num" + index).val(parseInt($("#num" + index).val()) - 1);
        $("#totalPrice" + index).val((price * $("#num" + index).val()).toFixed(2));
    }
})
$(document).on("click", ".btn_plus", function () {
    var index = $(".btn_plus").index(this) + 1
    var price = parseFloat($("#evenPrice" + index).html());
    $("#num" + index).val(parseInt($("#num" + index).val()) + 1);
    $("#totalPrice" + index).val((price * $("#num" + index).val()).toFixed(2));
})
$(document).on("blur", ".inputNum", function () {
    var index = $(".inputNum").index(this) + 1
    var price = parseFloat($("#evenPrice" + index).html());
    var num = $("#num" + index).val();
    if (num > 0) {
        $("#totalPrice" + index).val((price * num).toFixed(2));
    } else {
        $("#num" + index).val("1");
        $("#totalPrice" + index).val(price.toFixed(2));
    }
})
$(".chooseall").click(function () {
    $(".choose").attr("checked","checked");
})
$(document).on("click",".choose",function () {
    $(".chooseall").attr("checked","checked");

})

/*
< tr >
< td >
< input
type = "checkbox"
className = "chooseall" / >
    < /td>
<td>
    <img src="/Game/img/shopping/Assassins Creed Odyssey/title.jpg" width="75px" height="100px"/>
</td>
< td > < /td>
<td><span>¥&nbsp;</span><em id="evenPrice1">124.00</em></td>
< td >
< a
href = "#"
className = "btn btn_minus"
role = "button"
title = "减少" > < /a>
<input className="inputNum" id="num1" value="1" size="1">
    <a href="#" className="btn btn_plus" role="button" title="增加"></a>
</td>
< td > < span > ¥ & nbsp;
</span><input type="text" id="totalPrice1" value="124.00" size="3" disabled="disabled"/></td>
<td><a href="#">删除</a>&nbsp;&nbsp;<a href="#">加入收藏</a></td>
</tr>*/
