$(function () {
    function getUrlParam(key) {
        // 获取参数
        var url = window.location.search;
        // 正则筛选地址栏
        var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
        // 匹配目标参数
        var result = url.substr(1).match(reg);
        //返回参数值
        return result ? decodeURIComponent(result[2]) : null;
    }
    var id = getUrlParam("id");
    $.getJSON("/Game/user/getMsgById",{"id":id},function (data) {
        var nickname = data.nickName;
        var phone = data.phone;
        var email = data.email;
        if (nickname != ""){
            $("#bNickName").html(nickname)
        }else if (phone != ""){
            $("#bNickName").html(phone)
        }else if(email != ""){
            $("#bNickName").html(email)
        }
    })

    $("#modify").click(function(){
        $(".member-info").css("display","none");
        $("#modifyname").css("display","block");
    })
    $("#nameCancle").click(function () {
        $("#modifyname").css("display","none");
        $(".member-info").css("display","block");
    })
    $("#aConfirm").click(function () {
        $("#modifyname").css("display","none");
        $(".member-info").css("display","block");
    })
})