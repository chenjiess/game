<%--
  Created by IntelliJ IDEA.
  User: LENOVO
  Date: 2019/11/5
  Time: 8:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

    <title>登录页面</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">

    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/login0.css">
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript">
        $(function() {
            $("input[type=button]").click(function(){
                var username = $("input[type=text]").val();
                var password = $("input[type=password]").val();
                if (username == ""){
                    $("#error").html("用户名不能为空");
                }else if(password == ""){
                    $("#error").html("密码不能为空");
                }else{
                    $.ajax({
                        url:"user/login",
                        type:"post",
                        data:{"username":username,"password":password},
                        dataType:"json",
                        success:function(data){
                            if(data){
                                window.location.href = "stone.jsp";
                            }else{
                                $("#error").html("用户名或密码错误！");
                            }
                        }
                    })
                }
            });

            $(document).keydown(function(event){
                if(event.keydown == "13"){
                    $("input[type=button]").click();
                }
            });
        })

    </script>
</head>

<body>
            <header id="headNav">
                <div class="innerNav clear">
                    <a class="fl" href="#">
                        <img src="img/log.png" alt="stone"/>
                    </a>
                    <div class="headFont fr">
                        <span>您好，欢迎光临stone！ </span>
                    </div>
                </div>
            </header>
            <section id="secBod">
                <div class="innerBod clear">
                    <img class="fl" src="img/loadimg.png" alt=""/>
                    <div class="tableWrap fr">
            <form>
                <div class="tableTag clear">
                    <h3 class="fl">stone用户登录</h3>
                    <a class="fr" href="resign0.jsp">注册账号</a>
                </div>
                <div class="tableItem">
                    <i class="userHead"></i>
                    <input type="text" name="username" placeholder="邮箱/手机/用户名" required maxlength="30"/>
                </div>
                <div class="tableItem">
                    <i class="userLock"></i>
                    <input type="password" name="password" placeholder="密码" required/>
                </div>
                <div class="tableAuto clear">
                    <a class="autoMatic fl" href="#">
                        <input class="loadGiet" type="checkbox" />自动登录
                    </a>
                    <a class="fr" href="#">忘记密码</a>
                </div>
                    <input class="tableBtn" type="button" value="登录"/>
                    <span colspan="2" id="error" style="color: red" ></span>
                </form>
            <h2 class="moreWeb">更多合作网站账号登陆</h2>
            <div class="outType clear">
                <ul class="loadType clear">
                    <li class="fl"><a href="#"></a></li>
                    <li class="fl"><a href="#"></a></li>
                    <li class="fl"><a href="#"></a></li>
                    <li class="fl"><a href="#"></a></li>
                    <li class="loadMore fr">更多合作网站<i></i></li>
                </ul>
            </div>
        </div>
    </div>
</section>
            <footer id="footerNav">
                <p>
                    <a href="#">沪ICP备13044278号</a>|&nbsp;&nbsp;合字B1.B2-20130004&nbsp;&nbsp;|<a href="#">营业执照</a>
                </p>
                <p>Copyright © Stone Entertainment Studio  2007-2016，All Rights Reserved</p>
            </footer>
</body>
</html>

