<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2019/10/29
  Time: 10:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head id="Head1"><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
    stone - 会员登录
</title><link href="../css/login.css" rel="stylesheet" type="text/css" />
    <link href="../css/footer.css" rel="stylesheet" type="text/css" />
    <link href="../css/publiccontrols.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="" type="image/x-icon" />
    <script type="text/javascript" language="javascript" src=""></script>

    <script type="text/javascript" language="javascript" src="../js/script.ashx.js"></script>

    <script type="text/javascript" language="javascript" src="../js/login.js"></script>
    <meta name="keywords" content="stone - 会员登录," /><meta name="description" content="stone - 会员登录" /></head>
<body>
<center>
    <div id="Head" align="left">
        1.<!--<a class="logo" href="http://www.vancl.com"></a>-->
        <p>
            <a href="http://help.vancl.com/" target="_blank">帮助</a>
        </p>
    </div>
</center>
<div class="wrapper">
    <div style="width: 100%; height: 25px; position: relative;">
        <h3 style="font-size: 22px; color: #979797; font-weight: bolder; text-align: left;
                height: 25px; line-height: 25px; float: right; margin: 0 100px 0 0; width: 400px;
                position: relative;">
            stone 登录 <span style="font-size: 14px; font-weight: normal; position: absolute;
                    right: 0px; bottom: 0px;">没有账户免费<a id="gotoReg" href="javascript:void(0);" style="color: #b42025;
                        margin: 0px;">注册</a> </span>
        </h3>
    </div>
    <div class="login">
        <div class="tag">
            <ul id="tags">
                <li class="on">普通登录</li>
                <li>快速登录</li>
            </ul>
        </div>
        <!--tag-->
        <div id="shows">
            <div class="user_infor">
                <div id="vanclLoginError" class="tips">
                    用户名或者密码错误!
                </div>
                <div id="_uname" class="newtxt">
                    <label>
                        请输入用户名
                    </label>

                    <input id="vanclUserName" name="vanclUserName" type="text" value="" />
                </div>
                <div id="vanclUserNameError" class="tips">
                    用户名不能为空!
                </div>
                <div id="_upwd" class="newtxt">
                    <label>
                        请输入密码
                    </label>
                    <input id="vanclPassword" name="vanclPassword" type="password" class="inputtextcolor"
                           value="" />
                </div>
                <div id="vanclPasswordError" class="tips">
                    密码不能为空!
                </div>
                <p id='pValidate' style="display:none" class="newtxt">
                    <label style="color: #acacac">
                        验证码</label><input maxlength="6" style="width: 155px; height: 48px; border: 0 none;
                                text-indent: 10px;" id="calculatevalidate" name="calcultatevalidate" type="text"
                                          class="inputtextcolor" value="" />
                    <img id='img_validate' oldsrc='/Controls/CalculateValidateCode.ashx?key=Login' src=""
                         style="/* vertical-align: middle; */width: 145px; height: 53px; cursor: pointer;
                            position: absolute; right: 0px; top: 0px; z-index: 11;" />
                    <a style="cursor: pointer; float: right; line-height: 18px;" href="javascript:void(0)">
                        看不清?换一张</a>
                </p>
                <div id="validateError" style="display: none" class="tips">
                    验证码不能为空!
                </div>

                <div class="bt">

                    <span id="userThaw" style="display:none;"><a class="forget" href="">账户解冻</a></span>
                    3.<a class="forget" href="">找回密码</a>
                    <div class="clear">
                    </div>
                    <a id="vanclLogin" class="log" href="#">登 录</a>
                </div>
            </div>
            <!--
            ****************************************************
            快速登录区域----开始
            ****************************************************
             -->
            <div class="user_infor" style="display: none">
                <div id="_quickmobile" class="newtxt" style="width: 250px; float: left;">
                    <label>
                        输入手机号
                    </label>
                    <input id="_quickmobilenumber" maxlength="11" name="vanclUserName" type="text" value="" />
                </div>
                <a href="javascript:void(0);" class="asPhoneregBtn" id="getSmsCode" style="display: block">
                    获取短信验证码</a>
                <div class="asPhoneregBtn" style="display: none" id="sendedSmsCode">
                    已发送
                </div>
                <div class="asPhoneregBtn" style="display: none" id="sendingSmsCode">
                    正在发送验证码
                </div>
                <div id="err_phone" class="tips" style="width: 80%;">
                </div>
                <div id="_quickcode" class="newtxt" style="border: 0 none;">
                    <label style="width: 142px; height: 43px; line-height: 43px; border: solid 1px #cbcbcb;">
                        验证码
                    </label>
                    <input id="_quickpiccode" name="vanclUserName" type="text" value="" style="width: 142px;
                            border: solid 1px #cbcbcb; height: 43px; line-height: 43px;" />
                    <img id='_quickpiccodeimg' oldsrc='/Controls/CalculateValidateCode.ashx?key=Login'
                         src="" style="vertical-align: middle; width: 140px; height: 45px; position: absolute;
                            top: 0; left: 137px;" />
                    <a style="cursor: pointer; float: right; line-height: 18px;" href="javascript:void(0)">
                        看不清?换一张</a>
                </div>
                <div id="_quickpiccodevalidmsg" class="tips">
                    验证码不能为空!
                </div>
                <div id="_quickmobilecode" class="newtxt">
                    <label>
                        请输入手机验证码
                    </label>
                    <input id="_quickmobilevalidcode" name="vanclUserName" type="text" value="" />
                </div>
                <div id="_quickmobilecodemsg" class="tips">
                    请输入有效的手机号码!
                </div>
                <div class="bt">
                    <a id="_btnquicklogin" class="log" href="javascript:void(0);">登 录</a>
                </div>
            </div>
            <!-- 横线 -->
            <div class="lines">
            </div>


            <div class="glup">
                <span class="blank10"></span>
                <h2 class="hezuo">
                    使用合作网站账号登录凡客</h2>
                <span class="blank10"></span>
                <p class="qkimg">
                    <a href="" id="a_wxlogin" class="weixin">
                    </a><a class="xiaomi" onclick="openUrl('xiaomi')" href=""></a>
                    <a href="" onclick="openUrl('alipay')" class="zhifubao"></a><a
                        class="qq" href="" onclick="openUrl('qq')"></a><a class="qihu360"
                                                                          href="" onclick="openUrl('qihoo360')"></a><a class="weibo" href=""
                                                                                                                       onclick="openUrl('sinaweibo')"></a>

                <div class="clear">
                </div>
                <div class="blank10">
                </div>
                </p>
            </div>
            <!--tishi-->
        </div>
        <!--user_infor-->

    </div>
    <div class="advert">
        2. <!--<img src="" id="_advertImage" style="width: 465px" />-->
    </div>
    <!--shows-->
    <div style="clear: both;">
    </div>
</div>
<center>

    <!--Bottom End-->

    <script src="../js/login/quicklogin.js"></script>
</body>
</html>
