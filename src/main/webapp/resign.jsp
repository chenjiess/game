<%--
  Created by IntelliJ IDEA.
  User: dell
  Date: 2019/10/29
  Time: 10:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head id="Head1"><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><title>

    凡客诚品 - 会员免费注册
</title>
    <link rel="shortcut icon" href="" type="image/x-icon" /><link rel="stylesheet" type="text/css" href="" />

    <script type="text/javascript" language="javascript" src=""></script>

    <link href="../css/login/reg.css" type="text/css" rel="stylesheet" />
</head>
<body id="body" class="wrapper">
<div id="top" style="margin:10px 0 0 0;padding:0 0 10px 0; border-bottom:solid 1px #919191;">
    <img src="../img/log.png" height="48px"  width="181px"   alt="凡客诚品" title="凡客诚品" onclick="javascript:window.location.href=''" style="cursor:pointer" /></div>
<!--logo-->

<form method="post" action="" id="myform">
    <div class="aspNetHidden">

        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="02DF65DF" />
    </div>
    <span class="box15"></span>
    <div class="regist">
        <h1>
            注册新用户 <span>我已经注册，现在就<a href="login.jsp">登录</a></span>
        </h1>
        <div style="width: 100%; height: 10px; overflow: hidden; clear: both;">
        </div>
        <div class="content_left">
            <div class="tag clearfix">
                <h2>
                    选择注册方式：</h2>
                <ul id="tags">


                    <li class="tb-email" onclick="TabChange('email');return false;">使用Email注册</li>
                    <li class="tb-phone tb-phone-select" onclick="TabChange('phone');return false">使用手机号注册</li>
                </ul>
            </div>
            <style type="text/css">
                .newtxt
                {
                    *overflow:visible;
                    *z-index:100;
                }
            </style>
            <!--tag-->
            <div class="infor clearfix" style="overflow: inherit">

                <div class="email">
                    <ul id="v2regListInfo_bar">
                        <li id="lipiccode" style="display: block !important; zoom: 1;">
                            <div id="_quickcode" class="newtxt" style="border: 0 none; width: 415px; overflow: inherit;
                                zoom: 1;">
                                <label for="_quickpiccode" style="width: 260px; height: 43px; line-height: 43px;
                                    border: 1px solid rgb(203, 203, 203); z-index: 10;">
                                    验证码
                                </label>
                                <input id="_quickpiccode" name="vanclUserName" type="text" value="" style="width: 260px;
                                    border: solid 1px #cbcbcb; height: 43px; line-height: 43px;">
                                <img id="_quickpiccodeimg" oldsrc="/Controls/CalculateValidateCode.ashx?key=reg"
                                     src="/Controls/CalculateValidateCode.ashx?key=reg" style="cursor: pointer; vertical-align: middle;
                                    width: 140px; height: 45px; position: absolute; top: 0; left: 272px;">
                                <span class="v2reg_tips02" style="display: none;">请输入图片验证码</span>
                            </div>
                        </li>
                        <li id='liemail' style="display: none;">
                            <label>
                                Email地址：</label>
                            <div class="v2regListInfo_rtCont">
                                <input type="text" name="textfield" id="input_email" class="reginput_01" />
                                <span class="box3"></span><span class="v2reg_tips02"></span><span class="box5"></span>
                            </div>
                            <span class="Rclear"></span></li>
                        <li id='liphone' style="zoom: 1;">
                            <div class="v2regListInfo_rtCont" style="position: relative; zoom: 1;">

                                <div id="_umobile" class="newtxt" style="width: 260px; float: left;">
                                    <label>
                                        请输入手机号
                                    </label>
                                    <input maxlength='11' type="text" value="" name="textfield" id="input_phone" tabindex="1" />
                                </div>
                                <a href="#" class="asPhoneregBtn sprite_a" style="width: 140px;">获取短信验证码</a>
                                <div class="v2regList_Btnimg" style='display: none; width: 140px'>
                                    <img align="absmiddle" src="../img/loading.gif">
                                    正在发送...
                                </div>
                                <div class="iFloatingwindow">
                                    <p class="iFloatingIco">
                                        <img src="../img/regico.png"></p>
                                    <dl>
                                        <dd>
                                            <p class="inviteTips_ft" style="width: auto">
                                                <img src="../img/inviteico.gif">请输入验证码！</p>
                                        </dd>
                                        <dd>
                                            <img id='img_phonevalidate' src='/Controls/CalculateValidateCode.ashx?key=reg' width="145px"
                                                 height="53px" /></dd>
                                        <span class="clear"></span>
                                        <p style="padding-left: 63px;">
                                            <span class="ftgry6"><a class="grayft6" href="javascript:void(0)">看不清？换一张</a></span></p>
                                        <dd>
                                            <input type="text" class="iFloatingInput" value="" id="input_phonevalidate" /></dd>
                                        <dd class="inviteTips_img">
                                            <a href="javascript:void(0)">
                                                <img src="../img/inviteBtn.gif.png"></a></dd>
                                    </dl>
                                    <span class="clear"></span>
                                </div>
                                <p class="v2regBtn_02" name="ValidateIng">
                                    <img src="../img/no.gif" /></p>
                                <span class="box3"></span><span class="v2reg_tips02"></span><span class="box5"></span>
                            </div>
                            <span class="Rclear"></span></li>
                        <li id='liphonecode'>

                            <div class="v2regListInfo_rtCont" style="position: relative;">

                                <div id="_mcode" class="newtxt" style="width: 410px;">
                                    <label>
                                        填写手机验证码
                                    </label>
                                    <input name="textfield" id="input_phonecode" type="text" value="" maxlength="6" tabindex="2" />
                                </div>
                                <span class="box3"></span><span class="v2reg_tips01"></span><span class="box5"></span>
                            </div>
                            <span class="Rclear"></span></li>
                        <li>

                            <div class="v2regListInfo_rtCont" style="position: relative;">

                                <div id="_mpassword" class="newtxt" style="width: 410px;">
                                    <label>
                                        设定登录密码
                                    </label>
                                    <input maxlength="16" type="password" name="textfield" id="input_password" value=""
                                           tabindex="3" />
                                </div>
                                <span class="box3"></span>
                                <span class="v2reg_tips01"></span><span class="box5"></span>
                            </div>
                            <span class="Rclear"></span></li>
                        <li>

                            <div class="v2regListInfo_rtCont" style="position: relative;">

                                <div id="_mpassword2" class="newtxt" style="width: 410px;">
                                    <label>
                                        再次输入密码
                                    </label>
                                    <input maxlength="16" type="password" name="textfield" id="input_repassword" value=""
                                           tabindex="4" />
                                </div>
                                <span class="box3"></span><span class="v2reg_tips01"></span><span class="box5"></span>
                            </div>
                            <span class="Rclear"></span></li>
                        <li id='livalidatecode' style="height: 75px;">
                            <!-- display: none; -->


                            <div class="v2regListInfo_rtCont" style="position: relative;">
                                <div id="_yanzhenma" class="newtxt" style="width: 410px;">
                                    <label style="height: 45px; width: 150px; margin: 0px;">
                                        验证码
                                    </label>
                                    <input maxlength='6' type="text" name="textfield" id="input_validatecode" class="reginput_02"
                                           style="width: 150px;" />
                                    <img border="0" src="/Controls/CalculateValidateCode.ashx?key=reg" id="img_validatecode"
                                         style="right: -1px; position: absolute;">
                                </div>
                                <span class="box3"></span><span class="v2reg_tips02"></span><span class="box5"></span>
                            </div>
                            <span class="Rclear"></span></li>
                        <li>
                            <div class="v2reg_bt">
                                <span>
                                    <input id="chk_agreen" type="checkbox" onclick="chkfn.call(this)" />
                                    <label for="chk_agreen" style="width: 100%; float: none;">
                                        请阅读<a target="_blank" href="" class="reda10_ft12">《VANCL凡客诚品服务条款》</a></label></span>
                                <span class="box10"></span><a href="javascript:">
                                <div id="submitRegister_false" class="submitStyle" style="background: #9A9A9A; color: #FFFFFF;">
                                    立即注册</div>
                            </a><a href="javascript:" onclick="$('#submitRegister').click();return false;" style="display: none;">
                                <div id="submitRegister" class="submitStyle">
                                    立即注册</div>
                            </a>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--email-->
            </div>
            <!--infor-->
        </div>
        <!--content_left-->
        <div class="content_right">

        </div>
        <!--content_right-->
        <div class="clear">
        </div>
    </div>
    <!--regist-->
    <input name="hdn_RegisterType" id="hdn_RegisterType" type="hidden" value="email" />
    <input name="hdn_ResourceHref" id="hdn_ResourceHref_name" type="hidden" value="loginAndResign.html" />
    <!--wrapper-->


    <!--bottom-->

    <!--javascript-->


    <script type="text/JavaScript" src="../js/login/reg.js"
            language="javascript"></script>



    <script type="text/javascript" language="javascript" src="../js/public/union_websource.js"></script>
</body>
</html>
