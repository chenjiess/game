package com.kgc.mapper;

import com.kgc.entity.Order;

import java.util.List;

public interface OrderMapper {
    public List<Order> queryOrder();
}
