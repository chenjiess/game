package com.kgc.mapper;


import com.kgc.entity.Collection;
import com.kgc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    //  登录时验证用户名密码是否正确
    boolean queryByPhoneOrEmailAndPwd(String loginName, String password);
    // 查询所有用户
    public List<User> queryAllUser();
    //根据用户id删除用户
    public int delUser(@Param("id") int id);
    //根据用户id查询购物车
    List<Collection> queryShoppingCart(@Param("id") int id);

    public User queryUser(@Param("name")String name,@Param("pwd")String pwd);

    //用户注册，增加用户信息
    public  int addUser(@Param("user") User user);
}
