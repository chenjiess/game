package com.kgc.mapper;

import com.kgc.entity.Address;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressMapper {
    public List<Address> queryAddress(@Param("userId") int userId);
}
