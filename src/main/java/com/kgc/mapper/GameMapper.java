package com.kgc.mapper;

import com.kgc.entity.Collection;
import com.kgc.entity.Game;
import com.kgc.entity.GameNote;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GameMapper {
    // 获取游戏列表
    List<Game> queryGameList();
    // 根据游戏名查找游戏；
    List<Game> queryGameByName(String gameName);
    //表关联查询所有游戏
    public List<Game> queryGame();
    //游戏表查询游戏
    public List<Game> queryAllGame();
    //根据用户id查询收藏
    List<Collection> queryCollection(@Param("id") int id);
    //根据id查找游戏
    GameNote queryGameById(@Param("id") int id);
    //根据游戏id删除游戏
    public int delGame(@Param("id") int id);
    //根据id删除图片
    public int delPicture(@Param("id") int id);
    //根据id修改游戏价格
    public int updatePrice(@Param("id")int id,@Param("price")double price);
}
