package com.kgc.service.impl;


import com.kgc.entity.Order;
import com.kgc.mapper.OrderMapper;
import com.kgc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class OrderImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<Order> queryOrder() {
        return orderMapper.queryOrder();
    }
}
