package com.kgc.service.impl;

import com.kgc.entity.Collection;
import com.kgc.entity.User;
import com.kgc.mapper.UserMapper;
import com.kgc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> queryAllUser() {
        return userMapper.queryAllUser();
    }

    @Override
    public boolean delUser(int id) {
        return userMapper.delUser(id)==1?true:false;
    }

    @Override
    public List<Collection> queryShoppingCart(int id) {
        return userMapper.queryShoppingCart(id);
    }

    @Override
    public User queryUser(String name, String pwd) {
        return userMapper.queryUser(name, pwd);
    }

    @Override
    public boolean addUser(User user) {
        System.out.println("userMapper.addUser(user)"+userMapper.addUser(user));
        return userMapper.addUser(user)==1?true:false;
    }


}
