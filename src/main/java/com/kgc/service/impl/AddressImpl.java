package com.kgc.service.impl;

import com.kgc.entity.Address;
import com.kgc.mapper.AddressMapper;
import com.kgc.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class AddressImpl implements AddressService {
    @Autowired
    private AddressMapper addressMapper;
    @Override
    public List<Address> queryAddress(int userId) {
        return addressMapper.queryAddress(userId);
    }
}
