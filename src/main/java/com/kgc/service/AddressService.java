package com.kgc.service;

import com.kgc.entity.Address;

import java.util.List;

public interface AddressService {
    public List<Address> queryAddress(int userId);
}
