package com.kgc.service;

import com.kgc.entity.Collection;
import com.kgc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {
    public List<User> queryAllUser();

    public boolean delUser(int id);

    //根据用户id查询购物车
    List<Collection> queryShoppingCart(@Param("id") int id);

    public User queryUser(String name,String pwd);

    //增加一个用户信息
    public boolean addUser(User user);
}
