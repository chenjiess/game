package com.kgc.service;

import com.kgc.entity.Collection;
import com.kgc.entity.Game;

import java.util.List;

public interface GameService {
    //表关联查找所有游戏
    public List<Game> queryGame();
    //游戏表
    public List<Game> queryAllGame();
    //查找收藏
    List<Collection> queryCollection(int id);
    //根据id删除游戏
    public boolean delGame(int id);
    //根据id删除照片
    public boolean delPicture(int id);
    //修改价格
    public boolean updatePrice(int id,double price);
}
