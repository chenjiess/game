package com.kgc.controller;

import com.kgc.entity.Collection;
import com.kgc.entity.User;
import com.kgc.service.UserService;
import com.kgc.utils.Random6;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("user")
@ResponseBody
public class UserController {
    private int code;
    private String phoneNo;

    @Autowired
    public UserService userService;

    @RequestMapping("query1")
    public boolean queryByPhoneOrEmailAndPwd(@Param("userName") String loginName, @Param("password")String password){
    return  false;
    }
    @RequestMapping("queryAllUser")
    public List<User> queryAll(){
        return userService.queryAllUser();
    }
    @RequestMapping("delUser")
    public boolean delUser(int id){
        System.out.println("删除方法执行");
        return userService.delUser(id);
    }
    @RequestMapping("queryShoppingCart")
    public List<Collection> queryShoppingCart(int id) {
        System.out.println(userService.queryShoppingCart(id));
        return userService.queryShoppingCart(id);
    }
    @RequestMapping("login")
    public boolean login(String username, String password, HttpServletRequest req){
        User user = userService.queryUser(username, password);
        if (user != null) {
            HttpSession session = req.getSession();
            session.setAttribute("user", user);
            return true;
        }
        return false;
    }
    @RequestMapping("sendSms")
    public void sendSms(String phoneNo){
       code = Random6.getRandom();
       this.phoneNo=phoneNo;
       Random6.getCode(code,phoneNo);
    }
    @RequestMapping("resgin")
    public boolean Resgin(String phoneNo,int code,String pwd){
        User user = new User();
        user.setPhone(phoneNo.toString());
        user.setPassword(pwd);
        boolean flag = false;
        System.out.println(user.getPassword()+"--"+user.getPhone());
        flag = userService.addUser(user);
        if (phoneNo.equals(this.phoneNo) && this.code == code && flag == true){
            return true;
        }else{
            return false;
        }

    }
}
