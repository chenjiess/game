package com.kgc.controller;

import com.kgc.entity.Order;
import com.kgc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("order")
@Controller
@ResponseBody
public class OrderController {
    @Autowired
    private OrderService orderService;
    @RequestMapping("queryAll")
    public List<Order> queryAll(){
        System.out.println(orderService.queryOrder());
        return orderService.queryOrder();
    }
}
