package com.kgc.controller;

import com.kgc.entity.Address;
import com.kgc.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("address")
@ResponseBody
public class AddressController {

    @Autowired
    private AddressService addressService;


    @RequestMapping("queryAll")
    public List<Address> queryAll(int userId){
        return addressService.queryAddress(userId);
    }
}
