package com.kgc.entity;

public class Collection {
    private int id;
    private GameNote gameNote;
    private int status;
    private int amount;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GameNote getGameNote() {
        return gameNote;
    }

    public void setGameNote(GameNote gameNote) {
        this.gameNote = gameNote;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Collection{" +
                "id=" + id +
                ", gameNote=" + gameNote +
                ", status=" + status +
                ", amount=" + amount +
                '}';
    }

}
