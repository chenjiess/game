package com.kgc.entity;
/*
    游戏类
 */
public class Game {
    // 游戏id  ，  游戏名 ， 价格 ， 游戏类型 ， 开发公司 ， 游戏支持平台 ， 游戏发布时间 ，
    private int id;
    private String name;
    private double price;
    private String type;
    private String publisher;
    private  String developers;
    private String platFrom;
    private String issueDate;
    private String evaluation;
    private String content;
    private  int pictureId;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getDevelopers() {
        return developers;
    }

    public void setDevelopers(String developers) {
        this.developers = developers;
    }

    public String getPlatFrom() {
        return platFrom;
    }

    public void setPlatFrom(String platFrom) {
        this.platFrom = platFrom;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getPictureId() {
        return pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
                ", publisher='" + publisher + '\'' +
                ", developers='" + developers + '\'' +
                ", platFrom='" + platFrom + '\'' +
                ", issueDate='" + issueDate + '\'' +
                ", evaluation='" + evaluation + '\'' +
                ", content='" + content + '\'' +
                ", pictureId=" + pictureId +
                '}';
    }
}
